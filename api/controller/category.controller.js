const jwt = require('jsonwebtoken');
let categoryModel = require("../model/category.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addCategory = (req, res) => {
    let responceBody = req.body;
	console.log(responceBody);
	debugger;
    let categoryObject = {
        category: responceBody.category,
       
    }


    categoryModel.create(categoryObject, (err, result) => {
        console.log(result)
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'Category Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating category'
            }
            res.json(returnObject)
        }
    })
}

module.exports.getCategoryById = (req, res) => {
    let categoryId = req.query.id;
    //console.log(productId);
    categoryModel.find({ '_id': ObjectId(categoryId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let categoryObject = {
                _id: result[0]._id,
                imgData: base64data,
                category: result[0].category,
                
            }
            res.json(categoryObject);
        }
    })
}

module.exports.updateCategory = (req, res) => {
    let categoryData = req.body;
    console.log(categoryData);
    let id = categoryData.id
    categoryModel.updateOne({ '_id': ObjectId(id) }, { $set: categoryData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeCategory = (req, res) => {
    let categoryId = req.query.id;
    categoryModel.remove({ '_id': ObjectId(categoryId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getCategory = (req, res) => {
    categoryModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}