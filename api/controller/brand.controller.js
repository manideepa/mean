const jwt = require('jsonwebtoken');
let brandModel = require("../model/brand.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addBrand = (req, res) => {
    let responceBody = req.body;
	console.log(responceBody);
	debugger;
    let brandObject = {
        brand: responceBody.brand,
        email: responceBody.email,
        mobile: responceBody.mobile,
        warrantyTime: responceBody.warrantyTime,
        customerAssistance: responceBody.customerAssistance,
        warrantySupport: responceBody.warrantySupport,
        locations: responceBody.locations,
        rating: responceBody.rating
    }


    brandModel.create(brandObject, (err, result) => {
        console.log(result)
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'Brand Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating brand'
            }
            res.json(returnObject)
        }
    })
}

module.exports.getBrandById = (req, res) => {
    let brandId = req.query.id;
    //console.log(productId);
    brandModel.find({ '_id': ObjectId(brandId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let brandObject = {
                _id: result[0]._id,
                imgData: base64data,
                brand: result[0].brand,
                email: result[0].email,
                mobile: result[0].mobile,
                warrantyTime: result[0].warrantyTime,
                customerAssistance: result[0].customerAssistance,
                warrantySupport: result[0].warrantySupport,
                locations: result[0].locations,
                rating: result[0].rating
                
            }
            res.json(brandObject);
        }
    })
}

module.exports.updateBrand = (req, res) => {
    let brandData = req.body;
    console.log(brandData);
    let id = brandData.id
    brandModel.updateOne({ '_id': ObjectId(id) }, { $set: brandData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeBrand = (req, res) => {
    let brandId = req.query.id;
    brandModel.remove({ '_id': ObjectId(brandId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getBrands = (req, res) => {
    brandModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}