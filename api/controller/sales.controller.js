const jwt = require('jsonwebtoken');
let salesModel = require("../model/sales.model");
let userModel = require("../model/user.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addSales = (req, res) => {
    let responceBody = req.body;
    let retailerId = responceBody.retailerId;
    userModel.find({ '_id': ObjectId(retailerId) }, (err, result) => {
        if (err) {
            res.send("There is some issue")
        } else {
            
            pointPerAmount = 1;//result[0].pointPerAmount == undefined ? 1 : result[0].pointPerAmount;
            let salesObject = {
                imgData: responceBody.imgData,
                model: responceBody.model,
                brand: responceBody.brand,
                model_number: responceBody.model_number,
                price: responceBody.price,
                points: responceBody.price / pointPerAmount,
                date_of_purchase: responceBody.date_of_purchase,
                period_of_warranty: responceBody.period_of_warranty,
                retailer_product_id: responceBody.retailer_product_id,
                product_type: responceBody.product_type,
                serial_number: responceBody.serial_number,
                IMEI: responceBody.IMEI,
                customerMobile: responceBody.customerMobile,
                retailerId: retailerId,
                created_at: new Date()

            }
            salesModel.create(salesObject, (err, result) => {
                if (!err) {
                    let returnObject = {
                        'successCode': 2,
                        'message': 'Sales Successfully Created'
                    }
                    res.json(returnObject)
                } else {
                    let returnObject = {
                        'successCode': 1,
                        'message': 'There is some issue while creating sales'
                    }
                    res.json(returnObject)
                }
            })
        }
    })

}

module.exports.checkSales = (req, res) => {
    salesModel.findOne({ sales: req.body.sales }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles, id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidUser: false });
            }
        } else {
            return res.status(501).json({ message: "invalid User" })
        }
    })
}



module.exports.getSalesById = (req, res) => {
    let salesId = req.query.id;
    //console.log(productId);
    salesModel.find({ '_id': ObjectId(salesId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let salesObject = {
                _id: result[0]._id,
                model: result[0].model,
                model_number: result[0].model_number,
                imgData: base64data,
                brand: result[0].brand,
                price: result[0].price,
                date_of_purchase: result[0].date_of_purchase,
                period_of_warranty: result[0].period_of_warranty,
                retailer_product_id: result[0].retailer_product_id,
                product_type: result[0].product_type,
                serial_number: result[0].serial_number,
                IMEI: result[0].IMEI
            }
            res.json(salesObject);
        }
    })
}

module.exports.updateSales = (req, res) => {
    let salesData = req.body;
    console.log(salesData);
    let id = salesData.id
    salesModel.updateOne({ '_id': ObjectId(id) }, { $set: salesData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}

module.exports.updateNickName = (req, res) => {
    let salesData = req.body;
    console.log(salesData);
    let id = salesData.id
    salesModel.updateOne({ '_id': ObjectId(id) }, { $set: salesData }, (err, result) => {
        if (!err) {
            //res.json(result);
            if (!err) {
                let returnObject = {
                    'successCode': 2,
                    'message': 'Nick Name Successfully Created'
                }
                res.json(returnObject)
            } else {
                let returnObject = {
                    'successCode': 1,
                    'message': 'There is some issue while creating nick name'
                }
                res.json(returnObject)
            }
        }
    })
}


module.exports.removeSales = (req, res) => {
    let salesId = req.query.id;
    salesModel.remove({ '_id': ObjectId(salesId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getSales = (req, res) => {
    salesModel.find({}, (err, result) => {
        if (!err) {
            var resultData = [];
            for(var i = 0; i < result.length;i++){
                
                var dayExpire = 0;
                var purchaseDate = new Date(result[i].date_of_purchase); 
                var dateStr = result[i].date_of_purchase;

                if(result[i].period_of_warranty == '6 Months'){
                    
                    var days = 180;

                    var dayExpire = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + days));
                    
                     
                    var warrantyPeriod = 180;
                }else if(result[i].period_of_warranty == '1 Year'){
                    var days = 365;

                    var dayExpire = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + days));
                    var warrantyPeriod = 365;
                }else{
                    var days = 730;

                    var dayExpire = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + days));
                    var warrantyPeriod = 730;
                }

                // To calculate the time difference of two dates 
                var Difference_In_Time = dayExpire.getTime() - new Date(); 
  
                // To calculate the no. of days between two dates 
                var Difference_In_Days = Math.round(Difference_In_Time / (1000 * 3600 * 24)); 


                //console.log(purchaseDate+'**************'+dayExpire+'************'+warrantyPeriod+Difference_In_Days)
                resultData[i] = result[i];
                resultData[i]['warrantyExpire'] = Difference_In_Days;
                resultData[i]['warrantyPeriod'] = warrantyPeriod;
            
            }
            //console.log(resultData);
            res.json(resultData);
        }
    })
}