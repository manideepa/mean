const jwt = require('jsonwebtoken');
let warrantyRequestsModel = require("../model/warranty-requests.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addWarrantyRequests = (req, res) => {
    let responceBody = req.body;
	//debugger;
    let warrantyRequestsObject = {
        warrantyRequestId : 'WME',
        salesId: responceBody.salesId,
        customerId: responceBody.customerId,
        contactTime: responceBody.contactTime,
        createdAt: new Date(),
    }
    //var originaldata = Buffer.from(req.body.imgData, 'base64');
    //userObject.imgData = originaldata;

    warrantyRequestsModel.create(warrantyRequestsObject, (err, result) => {
        console.log(result)
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'Request Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating request'
            }
            res.json(returnObject)
        }
    })
}

module.exports.checkWarrantyRequests = (req, res) => {
    console.log(req);

    warrantyRequestsModel.findOne({ email: req.body.email }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles,pointPerAmount: doc.pointPerAmount ,id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidUser: false });
            }
        } else {
            return res.status(501).json({ message: "invalid User" })
        }
    })
}



module.exports.getWarrantyRequestsById = (req, res) => {
    let warrantyRequestId = req.query.id;

    warrantyRequestsModel.find({ '_id': ObjectId(warrantyRequestId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            //console.log(result[0].pointPerAmount,"kjdfk");
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let warrantyRequestObject = {
                id: result[0]._id,
                warrantyRequestId : 'WME',
                salesId: result[0].salesId,
                customerId: result[0].customerId,
                contactTime: result[0].contactTime,
            }
            res.json(warrantyRequestObject);
        }
    })
}

module.exports.updateWarrantyRequests = (req, res) => {
    let warrantyRequestData = req.body;
    let id = warrantyRequestData.id
    warrantyRequestsModel.updateOne({ '_id': ObjectId(id) }, { $set: warrantyRequestData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeWarrantyRequests = (req, res) => {
    let warrantyRequestId = req.query.id;
    warrantyRequestsModel.remove({ '_id': ObjectId(warrantyRequestId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getWarrantyRequests = (req, res) => {
	debugger;
    warrantyRequestsModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}
