const jwt = require('jsonwebtoken');
let executiveModel = require("../model/executive.model");
let userModel = require("../model/user.model");
const ObjectId = require('mongodb').ObjectId;
var Join = require('mongo-join').Join;

module.exports.addExecutive = (req, res) => {
    let responceBody = req.body;
    console.log(responceBody);
    //debugger;
    let roles = ['executive'];
    let executiveObject = {
            firstName: responceBody.firstName,
            lastName: responceBody.lastName,
            email: responceBody.email,
            address: responceBody.address,
            retailers: responceBody.retailers,
            roles: roles,
            password: executiveModel.hashPassword("Welcome@123")
        }
        //var originaldata = Buffer.from(req.body.imgData, 'base64');
        //userObject.imgData = originaldata;
    console.log("ECE", executiveObject);
    userModel.create(executiveObject, (err, result) => {
        console.log(result);
        if (!err) {
            lastInsertId = result._id;
            if (lastInsertId) {
                executiveData = {
                    retailerId: responceBody.retailers,
                    userId: lastInsertId
                };
                executiveModel.create(executiveData, (err, result) => {
                    if (!err) {
                        let returnObject = {
                            'successCode': 2,
                            'message': 'Executive Successfully Created'
                        }
                        res.json(returnObject)
                    } else {
                        let returnObject = {
                            'successCode': 1,
                            'message': 'There is some issue while creating executive'
                        }
                        res.json(returnObject)
                    }
                })
            }
        }
    })

    /*
     var dbo = db.db("warrantyme");

    dbo.collection("users").insertOne(executiveObject, function (err, result) {
     if (err) throw err;
     lastInsertId = result.insertedId;

         if (lastInsertId) {
             executiveData = {
                 retailerId: responceBody.retailers,
                 userId: lastInsertId
             };

             dbo.collection("executives").insertOne(executiveData, function (err, result) {
                 if (!err) {
             let returnObject = {
                 'successCode': 2,
                 'message': 'Executive Successfully Created'
             }
             res.json(returnObject)
         } else {
             let returnObject = {
                 'successCode': 1,
                 'message': 'There is some issue while creating executive'
             }
             res.json(returnObject)
         }
             });
         }

     });
     */
}

module.exports.checkExecutive = (req, res) => {
    console.log(req);

    executiveModel.findOne({ email: req.body.email }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles, id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidExecutive: false });
            }
        } else {
            return res.status(501).json({ message: "invalid Executive" })
        }
    })
}

module.exports.getExecutiveInfo = (req, res, next) => {
    verifyToken(req, res, next);
    var decotedToken = "";

    function verifyToken(req, res, next) {
        let token = req.query.token;
        jwt.verify(token, 'secret', (err, tokendata) => {
            console.log(tokendata)
            if (err) {
                return res.status(400).json({ message: "unauthorized request" })
            }
            if (tokendata) {
                decotedToken = tokendata;
                res.json(decotedToken);
                next();
            }
        })
    }
}

module.exports.getExecutiveById = (req, res) => {
    let executiveId = req.query.id;

    executiveModel.find({ '_id': ObjectId(executiveId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let executiveObject = {
                id: result[0]._id,
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                imgData: base64data,
                email: result[0].email,
                address: result[0].address,
                retailers: result[0].retailers,
                //password: result[0].password,

            }
            res.json(executiveObject);
        }
    })
}

module.exports.updateExecutive = (req, res) => {
    let executiveData = req.body;
    console.log(executiveData);
    let id = executiveData.id
    executiveModel.updateOne({ '_id': ObjectId(id) }, { $set: executiveData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeExecutive = (req, res) => {
    let executiveId = req.query.id;
    executiveModel.remove({ '_id': ObjectId(executiveId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getExecutives = (req, res) => {
    userModel.find({ roles: { $in: "executive" } }, (err, result) => {
        console.log("Resulttttttt", result)
        if (!err) {
            res.json(result);
        }
    })
}