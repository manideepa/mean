const jwt = require('jsonwebtoken');
let offersModel = require("../model/offers.model");
let userModel = require("../model/user.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addOffers = (req, res) => {
    let responceBody = req.body;
    let retailerId = responceBody.retailerId;
            userModel.find({ '_id': ObjectId(retailerId) }, (err, result) => {
                console.log(result);
                if (err) {
                    res.send("Retailer not found")
                } else {
                    
            let offersObject = {
                retailerId: retailerId,
                image: responceBody.image,
                product: responceBody.product,
                productDescription: responceBody.productDescription,
                actualPrice: responceBody.actualPrice,
                discountPrice: responceBody.discountPrice,
                //created_at: Date()
                }


            offersModel.create(offersObject, (err, result) => {
                console.log(result)
                if (!err) {
                    let returnObject = {
                        'successCode': 2,
                        'message': 'Offer Successfully Created'
                    }
                    res.json(returnObject)
                } else {
                    let returnObject = {
                        'successCode': 1,
                        'message': 'There is some issue while creating Offer'
                    }
                    res.json(returnObject)
                }
            })  
        }
    })
}

module.exports.getOffersById = (req, res) => {
    let offerId = req.query.id;
    //console.log(productId);
    offersModel.find({ '_id': ObjectId(offerId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let offersObject = {
                _id: result[0]._id,
                image: base64data,
                product: result[0].product,
                productDescription: result[0].productDescription,
                actualPrice: result[0].actualPrice,
                discountPrice: result[0].discountPrice,
                
            }
            res.json(offersObject);
        }
    })
}

module.exports.updateOffers = (req, res) => {
    let offersData = req.body;
    console.log(offersData);
    let id = offersData.id
    offersModel.updateOne({ '_id': ObjectId(id) }, { $set: offersData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeOffers = (req, res) => {
    let offersId = req.query.id;
    offersModel.remove({ '_id': ObjectId(offersId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getOffers = (req, res) => {
    offersModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}
