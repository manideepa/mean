const jwt = require('jsonwebtoken');
let loyaltyPointsModel = require("../model/loyalty-points.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addLoyaltyPoints= (req, res) => {
    let responceBody = req.body;
	console.log(req.body);
    let salesObject = {
        price: responceBody.price,
        points: responceBody.points,
    }
	
	
    //var originaldata = Buffer.from(req.body.imgData, 'base64');
    //userObject.imgData = originaldata;

    loyaltyPointsModel.create(salesObject, (err, result) => {
        
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'Loyalty Points Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating Loyalty Points'
            }
            res.json(returnObject)
        }
    })
}

module.exports.checkLoyaltyPoints = (req, res) => {
    loyaltyPointsModel.findOne({ sales: req.body.sales }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles, id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidUser: false });
            }
        } else {
            return res.status(501).json({ message: "invalid User" })
        }
    })
}



module.exports.getLoyaltyPointsById = (req, res) => {
    let loyaltyPointsId = req.query.id;
    //console.log(productId);
    loyaltyPointsModel.find({ '_id': ObjectId(loyaltyPointsId) }, (err, result) => {
        if (!err) {
            let salesObject = {
                _id: result[0]._id,
                price: result[0].price,
                points: result[0].points,
                	
            }
            res.json(salesObject);
        }
    })
}

module.exports.updateLoyaltyPoints = (req, res) => {
    let loyaltyPointsData = req.body;
    console.log(loyaltyPointsData);
    let id = loyaltyPointsData.id
    loyaltyPointsModel.updateOne({ '_id': ObjectId(id) }, { $set: loyaltyPointsData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.deleteLoyaltyPoints = (req, res) => {
    let loyaltyPointsId = req.query.id;
    loyaltyPointsModel.remove({ '_id': ObjectId(loyaltyPointsId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getLoyaltyPoints = (req, res) => {
    loyaltyPointsModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}