const jwt = require('jsonwebtoken');
let productModel = require("../model/product.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addProduct = (req, res) => {
    let responceBody = req.body;
	console.log(req.body);
    let productObject = {
        model: responceBody.model,
        brand: responceBody.brand,
        category: responceBody.category,
        model_number: responceBody.model_number,
        price: responceBody.price,
		date_of_purchase: responceBody.date_of_purchase,
		period_of_warranty: responceBody.period_of_warranty,
		retailer_product_id: responceBody.retailer_product_id,
		product_type: responceBody.product_type,
		serial_number: responceBody.serial_number,
		IMEI: responceBody.IMEI,
      
    }
	
	
    //var originaldata = Buffer.from(req.body.imgData, 'base64');
    //userObject.imgData = originaldata;

    productModel.create(productObject, (err, result) => {
        
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'Product Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating product'
            }
            res.json(returnObject)
        }
    })
}

module.exports.checkProduct = (req, res) => {
    productModel.findOne({ product: req.body.product }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles, id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidUser: false });
            }
        } else {
            return res.status(501).json({ message: "invalid User" })
        }
    })
}



module.exports.getProductById = (req, res) => {
    let productId = req.query.id;
    //console.log(productId);
    productModel.find({ '_id': ObjectId(productId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let productObject = {
                _id: result[0]._id,
                model: result[0].model,
                model_number: result[0].model_number,
                imgData: base64data,
                brand: result[0].brand,
                category: result[0].category,
                price: result[0].price,
				date_of_purchase: result[0].date_of_purchase,
				period_of_warranty: result[0].period_of_warranty,
				retailer_product_id: result[0].retailer_product_id,
				product_type: result[0].product_type,
				serial_number: result[0].serial_number,
				IMEI: result[0].IMEI	
            }
            res.json(productObject);
        }
    })
}

module.exports.updateProduct = (req, res) => {
    let productData = req.body;
    console.log(productData);
    let id = productData.id
    productModel.updateOne({ '_id': ObjectId(id) }, { $set: productData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeProduct = (req, res) => {
    let productId = req.query.id;
    productModel.remove({ '_id': ObjectId(productId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getProducts = (req, res) => {
    productModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}