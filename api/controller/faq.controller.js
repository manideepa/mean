const jwt = require('jsonwebtoken');
let faqModel = require("../model/faq.model");
let brandModel = require("../model/brand.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addFaq = (req, res) => {
    let responceBody = req.body;
	console.log(responceBody);
	debugger;
    let faqObject = {
        brand: responceBody.brand,
        question: responceBody.question,
        answer: responceBody.answer,
        }


    faqModel.create(faqObject, (err, result) => {
        console.log(result)
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'faq Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating faq'
            }
            res.json(returnObject)
        }
    })
}

module.exports.getFaqById = (req, res) => {
    let faqId = req.query.id;
    //console.log(productId);
    faqModel.find({ '_id': ObjectId(faqId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let faqObject = {
                _id: result[0]._id,
                imgData: base64data,
                brand: result[0].brand,
                question: result[0].question,
                answer: result[0].answer
                
            }
            res.json(faqObject);
        }
    })
}

module.exports.updateFaq = (req, res) => {
    let faqData = req.body;
    console.log(faqData);
    let id = faqData.id
    faqModel.updateOne({ '_id': ObjectId(id) }, { $set: faqData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeFaq = (req, res) => {
    let faqId = req.query.id;
    faqModel.remove({ '_id': ObjectId(faqId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getFaqs = (req, res) => {
    faqModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}

module.exports.getBrands = (req, res) => {
    brandModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}