const jwt = require('jsonwebtoken');
let customerModel = require("../model/customers.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addCustomer = (req, res) => {
    console.log(req.body);
    let responceBody = req.body;
    customerModel.create(responceBody,(err,result)=> {
        console.log(result);
        res.json(result)
    })
}   

module.exports.getCustomers = (req,res) => {
    customerModel.find({},(err,result) => {
        res.json(result)
    })
} 

module.exports.getCustomer = (req,res) => {
    console.log(req)
    let mobile = req.query.mobile;
    customerModel.findOne({'mobile': mobile} ,(err,result)=> {
        res.json(result)
    })
}