const jwt = require('jsonwebtoken');
let userModel = require("../model/user.model");
let salesModel = require("../model/sales.model");
let customersModel = require("../model/customers.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addUser = (req, res) => {
    let responceBody = req.body;
	//debugger;
    let userObject = {
        firstName: responceBody.firstName,
        lastName: responceBody.lastName,
        email: responceBody.email,
        login: responceBody.login,
        roles: responceBody.roles,
		lattitude: responceBody.lattitude,
		longitude: responceBody.longitude,
		additional_one: responceBody.additional_one,
		additional_two: responceBody.additional_two,
		additional_three: responceBody.additional_three,
        additional_four: responceBody.additional_four,
        pointPerAmount: responceBody.pointPerAmount,
        password: userModel.hashPassword("Welcome@123")
    }
    //var originaldata = Buffer.from(req.body.imgData, 'base64');
    //userObject.imgData = originaldata;

    userModel.create(userObject, (err, result) => {
        console.log(result)
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'User Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating user'
            }
            res.json(returnObject)
        }
    })
}

module.exports.checkUser = (req, res) => {
    console.log(req);

    userModel.findOne({ email: req.body.email }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles,pointPerAmount: doc.pointPerAmount ,id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidUser: false });
            }
        } else {
            return res.status(501).json({ message: "invalid User" })
        }
    })
}

module.exports.getUserInfo = (req, res, next) => {
    verifyToken(req, res, next);
    var decotedToken = "";

    function verifyToken(req, res, next) {
        let token = req.query.token;
        jwt.verify(token, 'secret', (err, tokendata) => {
            console.log(tokendata)
            if (err) {
                return res.status(400).json({ message: "unauthorized request" })
            }
            if (tokendata) {
                decotedToken = tokendata;
                res.json(decotedToken);
                next();
            }
        })
    }
}

module.exports.getUserById = (req, res) => {
    let userId = req.query.id;

    userModel.find({ '_id': ObjectId(userId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            //console.log(result[0].pointPerAmount,"kjdfk");
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let userObject = {
                id: result[0]._id,
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                imgData: base64data,
                email: result[0].email,
                login: result[0].login,
                roles: result[0].roles,
				//password: result[0].password,
				lattitude: result[0].lattitude,
				longitude: result[0].longitude,
				additional_one: result[0].additional_one,
				additional_two: result[0].additional_two,
                additional_three: result[0].additional_three,
                pointPerAmount: result[0].pointPerAmount,
				additional_four: result[0].additional_four
            }
            res.json(userObject);
        }
    })
}

module.exports.updateUser = (req, res) => {
    let userData = req.body;
    let id = userData.id
    userModel.updateOne({ '_id': ObjectId(id) }, { $set: userData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeUser = (req, res) => {
    let userId = req.query.id;
    userModel.remove({ '_id': ObjectId(userId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getUsers = (req, res) => {
	debugger;
    userModel.find({}, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}
module.exports.getRetailers = (req, res) => {
    debugger;
    roles="retailer";
    userModel.find({ 'roles': "retailer" }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}

module.exports.getRetailersCount = (req, res) => {
    roles="retailer";
    /*userModel.find({ 'roles': "retailer" }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
    */
    userModel.countDocuments({'roles': "retailer"}, function(err, count) {
        if (err) { return handleError(err) } //handle possible errors
        console.log('ssssssss');
        res.json(count);
    })
}

module.exports.getSalesCount = (req, res) => {
    
    salesModel.countDocuments({}, function(err, count) {
        if (err) { return handleError(err) } //handle possible errors
        console.log('eeeeeeee');
        res.json(count);
    })
}

module.exports.getCustomersCount = (req, res) => {
    
    customersModel.countDocuments({}, function(err, count) {
        if (err) { return handleError(err) } //handle possible errors
        console.log('CCCCCCCCC');
        res.json(count);
    })
}