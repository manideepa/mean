const jwt = require('jsonwebtoken');
let retailerModel = require("../model/retailer.model");
const userModel = require("../model/user.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addRetailer = (req, res) => {
    let responceBody = req.body;
    console.log(responceBody);
    debugger;
    let userObject = {
            retailerName: responceBody.retailerName,
            status: responceBody.status,

        }
        //var originaldata = Buffer.from(req.body.imgData, 'base64');
        //userObject.imgData = originaldata;

    retailerModel.create(userObject, (err, result) => {
        console.log(result)
        if (!err) {
            let returnObject = {
                'successCode': 2,
                'message': 'User Successfully Created'
            }
            res.json(returnObject)
        } else {
            let returnObject = {
                'successCode': 1,
                'message': 'There is some issue while creating user'
            }
            res.json(returnObject)
        }
    })
}

module.exports.checkUser = (req, res) => {
    retailerModel.findOne({ email: req.body.email }, (err, doc) => {
        if (doc) {
            if (doc.isValid(req.body.password)) {
                console.log("docccccccccccccc", doc)
                let token = jwt.sign({ firstName: doc.firstName + " " + doc.lastName, roles: doc.roles, id: doc._id }, 'secret', { expiresIn: "1hr" });
                return res.status(200).json(token);
            } else {
                return res.status(501).json({ isValidUser: false });
            }
        } else {
            return res.status(501).json({ message: "invalid User" })
        }
    })
}

module.exports.getRetailerInfo = (req, res, next) => {
    verifyToken(req, res, next);
    var decotedToken = "";

    function verifyToken(req, res, next) {
        let token = req.query.token;
        jwt.verify(token, 'secret', (err, tokendata) => {
            console.log(tokendata)
            if (err) {
                return res.status(400).json({ message: "unauthorized request" })
            }
            if (tokendata) {
                decotedToken = tokendata;
                res.json(decotedToken);
                next();
            }
        })
    }
}

module.exports.getRetailerById = (req, res) => {
    let userId = req.query.id;

    retailerModel.find({ '_id': ObjectId(userId) }, (err, result) => {
        if (!err) {
            let data = result[0].imgData;
            console.log(result);
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let userObject = {
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                imgData: base64data,
                email: result[0].email,
                login: result[0].login,
                roles: result[0].roles
            }
            res.json(userObject);
        }
    })
}

module.exports.updateRetailer = (req, res) => {
    let userData = req.body;
    let id = userData.id
    retailerModel.updateOne({ '_id': ObjectId(id) }, { $set: userData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.removeRetailer = (req, res) => {
    let userId = req.query.id;
    retailerModel.remove({ '_id': ObjectId(userId) }, (err, result) => {
        res.json(result);
    })
}

module.exports.getRetailers = (req, res) => {
    userModel.find({ roles: { $in: "retailer" } }, (err, result) => {
        console.log("Resulttttttt", result)
        if (!err) {
            res.json(result);
        }
    })
}