let serviceCenterModel = require("../model/service-center.model");
const ObjectId = require('mongodb').ObjectId;

module.exports.addServiceCenter = (req,res) => {
    let reqBody = req.body;
   serviceCenterModel.create(reqBody,(err,result)=>{
       res.json(result)
   });

}
module.exports.getServiceCenters = (req,res) => {
    serviceCenterModel.find({},(err,result) => {
        if(!err) {
            res.json(result);
        }
    })
}
module.exports.getServiceCenterById = (req, res) => {
    let serviceCenterId = req.query.id;

    serviceCenterModel.find({ '_id': ObjectId(serviceCenterId) }, (err, result) => {
        if (!err) {
			console.log(result);
            let data = result[0].imgData;
            //console.log(result[0].pointPerAmount,"kjdfk");
            var base64data = result[0].imgData ? Buffer.from(data, 'binary').toString('base64') : "";
            let userObject = {
                id: result[0]._id,
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                imgData: base64data,
                email: result[0].email,
                login: result[0].login,
                brands: result[0].brands,
				password: result[0].password,
            }
            res.json(userObject);
        }
    })
}

module.exports.updateServiceCenter = (req, res) => {
    let serviceCenterData = req.body;
    let id = serviceCenterData.id
    serviceCenterModel.updateOne({ '_id': ObjectId(id) }, { $set: serviceCenterData }, (err, result) => {
        if (!err) {
            res.json(result);
        }
    })
}


module.exports.deleteServiceCenter = (req, res) => {
    let serviceCenterId = req.query.id;
    serviceCenterModel.remove({ '_id': ObjectId(serviceCenterId) }, (err, result) => {
        res.json(result);
    })
}
