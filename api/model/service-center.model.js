const mongoose = require("mongoose")
const schema = mongoose.Schema;

let serviceCenterSchema = new schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String
    },
    login: {
        type: String
    },
    password: {
        type: String
    },
    lattitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    address: {
        type: String
    }
})

const serviceCenterModel = mongoose.model('serviceCenters',serviceCenterSchema);
module.exports = serviceCenterModel;