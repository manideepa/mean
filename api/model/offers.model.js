const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let offersSchema = new schema({
    Id: {
        type: String
    },
    retailerId: {
        type: String
    },
    image: {
        type: String
    },
    product: {
        type: String
    },
    productDescription: {
        type: String
    },
    actualPrice: {
        type: String
    },
    discountPrice: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: String
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    }
});



var offersModel = mongoose.model("offers",offersSchema);



module.exports = offersModel;