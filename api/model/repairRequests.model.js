const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let repairRequestsSchema = new schema({
    Id: {
        type: Number
    },
    customer_Id: {
        type: int32
    },
    sales_Id: {
        type: int32
    },
    repair_centre_Id: {
        type: int32
    },
	lattitude: {
        type: String
    },
    longitude: {
        type: String
    },
	repair_price: {
        type: String
    },
	response: {
        type: String
    },
	cs_Id: {
        type: int32
    },
    status: {
        type: int32
    },
    updated_by: {
        type: int32
    },
    created_at: {
        type: datetime
    },
    updated_at: {
        type: datetime
    }
});



var repairRequestsModel = mongoose.model("repairRequests", repairRequestsSchema);



module.exports = repairRequestsModel;