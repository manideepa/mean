const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let userSchema = new schema({
    userId: {
        type: Number
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String
    },
    login: {
        type: String
    },
    roles: {
        type: Array
    },
    password: {
        type: String
    },
    imgData: {
        type: Buffer
    },
    lattitude: {
        type: String
    },
    longitude: {
        type: String
    },
    additional_one: {
        type: String
    },
    additional_two: {
        type: String
    },
    additional_three: {
        type: String
    },
    additional_four: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    },
    pointPerAmount: {
        type: Number
    },
    retailers: {
        type: String
    }
});

userSchema.statics.hashPassword = function hashPassword(password) {
    return bcrypt.hashSync(password, 10)
}

userSchema.methods.isValid = function(hashedPassword) {
    return bcrypt.compareSync(hashedPassword, this.password);
}

var userModel = mongoose.model("users", userSchema);



module.exports = userModel;