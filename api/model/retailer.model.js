const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let retailerSchema = new schema({
    retailerId: {
        type: Number
    },
    userId: {
        type: String
    },
    retailerName: {
        type: String
    },
    retailerImage: {
        type: String
    },
    brandId: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    }
});



var retailerModel = mongoose.model("retailers", retailerSchema);



module.exports = retailerModel;