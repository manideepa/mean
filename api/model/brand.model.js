const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let brandSchema = new schema({
    brandId: {
        type: Number
    },
    brand: {
        type: String
    },
    brandImage: {
        type: String
    },
    email: {
        type: String
    },
    mobile: {
        type: String
    },
    warrantyTime: {
        type: String
    },
    customerAssistance: {
        type: String
    },
    warrantySupport: {
        type: String
    },
    locations: {
        type: String
    },
    rating: {
        type: String
    },
    brandImage: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
});



var brandModel = mongoose.model("brands", brandSchema);



module.exports = brandModel;