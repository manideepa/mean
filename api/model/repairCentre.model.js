const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let repairCentreSchema = new schema({
    Id: {
        type: Number
    },
    user_Id: {
        type: String
    },
    repair_centre_name: {
        type: String
    },
    repair_centre_image: {
        type: String
    },
	repair_centre_mobile: {
        type: int32
    },
   
    status: {
        type: int32
    },
    updated_by: {
        type: int32
    },
    created_at: {
        type: datetime
    },
    updated_at: {
        type: datetime
    }
});



var repairCentreModel = mongoose.model("repairCentre", repairCentreSchema);



module.exports = repairCentreModel;