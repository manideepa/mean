const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let executiveSchema = new schema({
    executiveId: {
        type: Number
    },
    userId: {
        type: String
    },
    retailerId: {
        type: String
    },
    retailers: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    }
});

executiveSchema.statics.hashPassword = function hashPassword(password) {
    return bcrypt.hashSync(password, 10)
}

executiveSchema.methods.isValid = function(hashedPassword) {
    return bcrypt.compareSync(hashedPassword, this.password);
}

var executiveModel = mongoose.model("executives", executiveSchema);
module.exports = executiveModel;