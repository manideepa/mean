const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let brandServiceCentreSchema = new schema({
    Id: {
        type: Number
    },
    brand_Id: {
        type: String
    },
    lattitude: {
        type: String
    },
    longitude: {
        type: String
    },
	email: {
        type: String
    },
    mobile: {
        type: int32
    },
    terms_conditions: {
        type: String
    },
    status: {
        type: int32
    },
    updated_by: {
        type: int32
    },
    created_at: {
        type: datetime
    },
    updated_at: {
        type: datetime
    }
});



var brandServiceCentreModel = mongoose.model("brandServiceCentres", brandServiceCentreSchema);



module.exports = brandServiceCentreModel;