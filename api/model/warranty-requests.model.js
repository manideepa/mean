const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let warrantyRequestsSchema = new schema({
    warrantyRequestId: {
        type: String
    },
    salesId: {
        type: String
    },
    customerId: {
        type: String
    },
    contactTime: {
        type: String
    },
    createdAt: {
        type: String
    },
    updatedAt: {
        type: String
    }
});



var warrantyRequestsModel = mongoose.model("warrantyRequests", warrantyRequestsSchema);



module.exports = warrantyRequestsModel;