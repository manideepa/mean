const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let faqSchema = new schema({
    Id: {
        type: Number
    },
    brand: {
        type: String
    },
    question: {
        type: String
    },
    answer: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: String
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    }
});



var faqModel = mongoose.model("faqs", faqSchema);



module.exports = faqModel;