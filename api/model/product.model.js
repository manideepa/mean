const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let productSchema = new schema({
    productId: {
        type: Number
    },
	serial_number: {
        type: String
    },
	IMEI: {
        type: String
    },
	model: {
        type: String
    },
    model_number: {
        type: String
    },
	date_of_purchase: {
        type: String
    },
	period_of_warranty: {
        type: Array
    },
	retailer_product_id: {
        type: String
    },
    brand: {
        type: String
    },
    category: {
        type: String
    },
    SKU: {
        type: String
    },
    unique_WME_Id: {
        type: String
    },
    price: {
        type: String
    },
	product_type: {
        type: Array
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    }
});



var productModel = mongoose.model("products", productSchema);



module.exports = productModel;