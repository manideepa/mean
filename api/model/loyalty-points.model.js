const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let loyaltyPointsSchema = new schema({
    price: {
        type: Number
    },
	points: {
        type: String
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    }
});



var loyaltyPointsModel = mongoose.model("loyaltyPoints", loyaltyPointsSchema);



module.exports = loyaltyPointsModel;