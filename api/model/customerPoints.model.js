const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let customerPointsSchema = new schema({
    Id: {
        type: Number
    },
    customer_Id: {
        type: int32
    },
    loyalty_points: {
        type: int32
    },
    redeemed_points: {
        type: int32
    },
	sales_id: {
        type: int32
    },
    remaining_points: {
        type: int32
    },
    status: {
        type: int32
    },
    updated_by: {
        type: int32
    },
    created_at: {
        type: datetime
    },
    updated_at: {
        type: datetime
    }
});



var customerPointsModel = mongoose.model("customerPoints", customerPointsSchema);



module.exports = brandServiceCentreModel;