const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let categorySchema = new schema({
    categoryId: {
        type: Number
    },
    category: {
        type: String
    },
    imgData: {
        type: Buffer
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
});



var categoryModel = mongoose.model("categories", categorySchema);



module.exports = categoryModel;