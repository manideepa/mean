const mongoose = require("mongoose")
const schema = mongoose.Schema;

const customerSchema = new schema({
    id: {
        type: String
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    mobile: {
        type: Number
    },
    email: {
        type: String
    },
    points: {
        type: Number
    }
})

const customerModel = mongoose.model("customers", customerSchema);
module.exports = customerModel;