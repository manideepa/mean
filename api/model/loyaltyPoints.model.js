const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let loyaltyPointsSchema = new schema({
    Id: {
        type: Number
    },
    price: {
        type: int32
    },
    points: {
        type: String
    },
	retailer_Id: {
        type: String
    },
    created_at: {
        type: datetime
    },
    start_date: {
        type: date
    },
	end_date: {
        type: date
    },
    event: {
        type: String
    },
    status: {
        type: int32
    },
    updated_by: {
        type: int32
    },
    created_at: {
        type: datetime
    },
    updated_at: {
        type: datetime
    }
});



var loyaltyPointsModel = mongoose.model("loyaltyPoints", loyaltyPointsSchema);



module.exports = loyaltyPointsModel;