const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let salesSchema = new schema({
    nickName: {
        type: String
    },
    productId: {
        type: Number
    },
    serial_number: {
        type: String
    },
    IMEI: {
        type: String
    },
    model: {
        type: String
    },
    model_number: {
        type: String
    },
    date_of_purchase: {
        type: String
    },
    period_of_warranty: {
        type: String
    },
    retailer_product_id: {
        type: String
    },
    brand: {
        type: String
    },
    price: {
        type: String
    },
    product_type: {
        type: String
    },
    customerMobile: {
        type: String
    },
    retailerId: {
        type: String
    },
    warrantyExpire: {
        type: String
    },
    warrantyPeriod: {
        type: String
    },
    status: {
        type: Array
    },
    updated_by: {
        type: Buffer
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    },
    points: {
        type: Number
    },
    imgData: {
        type: Buffer
    }
});



var salesModel = mongoose.model("sales", salesSchema);



module.exports = salesModel;