const mongoose = require("mongoose")
const schema = mongoose.Schema;

const bcrypt = require("bcrypt")


let customerTicketsSchema = new schema({
    Id: {
        type: Number
    },
    customer_Id: {
        type: int32
    },
    customer_support_Id: {
        type: int32
    },
    topic: {
        type: String
    },
	description: {
        type: String
    },
    status: {
        type: int32
    },
    updated_by: {
        type: int32
    },
    created_at: {
        type: datetime
    },
    updated_at: {
        type: datetime
    }
});



var customerTicketsModel = mongoose.model("customerTickets", customerTicketsSchema);



module.exports = customerTicketsModel;