const express = require("express");
const categoryRouter = express.Router();

const categoryController = require('../controller/category.controller');

categoryRouter.post('/addCategory', categoryController.addCategory);
//productRouter.post('/checkProduct', categoryController.checkProduct);
categoryRouter.get('/getCategory', categoryController.getCategory);
categoryRouter.get('/getCategoryById', categoryController.getCategoryById);
categoryRouter.put('/updateCategory', categoryController.updateCategory);
categoryRouter.delete('/deleteCategory', categoryController.removeCategory);

module.exports = categoryRouter;