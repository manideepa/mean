const express = require("express");
const serviceCenterRouter = express.Router();

const serviceCenterController = require('../controller/service-center.controller');


serviceCenterRouter.get('/getServiceCenterById',serviceCenterController.getServiceCenterById);
serviceCenterRouter.delete('/deleteServiceCenter',serviceCenterController.deleteServiceCenter);
serviceCenterRouter.put('/updateServiceCenter',serviceCenterController.updateServiceCenter);
serviceCenterRouter.post('/addServiceCenter',serviceCenterController.addServiceCenter);
serviceCenterRouter.get('/getServiceCenters',serviceCenterController.getServiceCenters)

module.exports = serviceCenterRouter;