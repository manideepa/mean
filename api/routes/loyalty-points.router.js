const express = require("express");
const loyaltyPointsRouter = express.Router();

const loyaltyPointsController = require('../controller/loyalty-points.controller');

loyaltyPointsRouter.post('/addLoyaltyPoints', loyaltyPointsController.addLoyaltyPoints);
loyaltyPointsRouter.post('/checkLoyaltyPoints', loyaltyPointsController.checkLoyaltyPoints);
loyaltyPointsRouter.get('/getLoyaltyPoints', loyaltyPointsController.getLoyaltyPoints);
loyaltyPointsRouter.get('/getLoyaltyPointsById', loyaltyPointsController.getLoyaltyPointsById);
loyaltyPointsRouter.put('/updateLoyaltyPoints', loyaltyPointsController.updateLoyaltyPoints);
loyaltyPointsRouter.delete('/deleteLoyaltyPoints', loyaltyPointsController.deleteLoyaltyPoints);

module.exports = loyaltyPointsRouter;