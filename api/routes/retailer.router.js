const express = require("express");
const retailerRouter = express.Router();

const retailerController = require('../controller/retailer.controller');

retailerRouter.post('/addRetailer', retailerController.addRetailer);
retailerRouter.get('/getRetailers', retailerController.getRetailers)
    //retailerRouter.post('/checkRetailer', retailerController.checkUser);
    //retailerRouter.get('/getRetailer', retailerController.getRetailerInfo);
    //retailerRouter.get('/getRetailers', retailerController.getRetailer);
    //retailerRouter.get('/gerUserById', userController.getUserById);
    //retailerRouter.put('/updateUser', userController.updateUser);
    //retailerRouter.delete('/deleteUser', userController.removeUser);

module.exports = retailerRouter;