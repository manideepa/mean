const express = require("express");
const salesRouter = express.Router();

const salesController = require('../controller/sales.controller');

salesRouter.post('/addSales', salesController.addSales);
salesRouter.post('/checkSales', salesController.checkSales);
salesRouter.get('/getSales', salesController.getSales);
salesRouter.get('/getSalesById', salesController.getSalesById);
salesRouter.put('/updateSales', salesController.updateSales);
salesRouter.put('/updateNickName', salesController.updateNickName);
salesRouter.delete('/deleteSales', salesController.removeSales);

module.exports = salesRouter;