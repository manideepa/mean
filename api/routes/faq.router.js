const express = require("express");
const faqRouter = express.Router();

const faqController = require('../controller/faq.controller');

faqRouter.post('/addFaq', faqController.addFaq);
//productRouter.post('/checkProduct', faqController.checkProduct);
faqRouter.get('/getFaqs', faqController.getFaqs);
faqRouter.get('/getFaqById', faqController.getFaqById);
faqRouter.put('/updateFaq', faqController.updateFaq);
faqRouter.delete('/deleteFaq', faqController.removeFaq);

module.exports = faqRouter;