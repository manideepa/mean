const express = require("express");
const userRouter = express.Router();

const userController = require('../controller/user.controller');

userRouter.post('/addUser', userController.addUser);
userRouter.post('/checkUser', userController.checkUser);
userRouter.get('/getUser', userController.getUserInfo);
userRouter.get('/getUsers', userController.getUsers);
userRouter.get('/getRetailers', userController.getRetailers);

userRouter.get('/gerUserById', userController.getUserById);
userRouter.put('/updateUser', userController.updateUser);
userRouter.delete('/deleteUser', userController.removeUser);
userRouter.get('/getRetailersCount', userController.getRetailersCount);
userRouter.get('/getSalesCount', userController.getSalesCount);
userRouter.get('/getCustomersCount', userController.getCustomersCount);
module.exports = userRouter;