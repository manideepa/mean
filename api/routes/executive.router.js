const express = require("express");
const executiveRouter = express.Router();

const executiveController = require('../controller/executive.controller');

executiveRouter.post('/addExecutive', executiveController.addExecutive);
executiveRouter.post('/checkExecutive', executiveController.checkExecutive);
executiveRouter.get('/getExecutive', executiveController.getExecutiveInfo);
executiveRouter.get('/getExecutives', executiveController.getExecutives);
executiveRouter.get('/gerExecutiveById', executiveController.getExecutiveById);
executiveRouter.put('/updateExecutive', executiveController.updateExecutive);
executiveRouter.delete('/deleteExecutive', executiveController.removeExecutive);

module.exports = executiveRouter;