const express = require("express");
const offersRouter = express.Router();

const offersController = require('../controller/offers.controller');

offersRouter.post('/addOffers', offersController.addOffers);
//productRouter.post('/checkProduct', faqController.checkProduct);
offersRouter.get('/getOffers', offersController.getOffers);
offersRouter.get('/getOffersById', offersController.getOffersById);
offersRouter.put('/updateOffers', offersController.updateOffers);
offersRouter.delete('/deleteOffers', offersController.removeOffers);

module.exports = offersRouter;