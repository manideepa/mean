const express = require("express");
const warrantyRequestsRouter = express.Router();

const warrantyRequestsController = require('../controller/warranty-requests.controller');

warrantyRequestsRouter.post('/addWarrantyRequests', warrantyRequestsController.addWarrantyRequests);
warrantyRequestsRouter.post('/checkWarrantyRequests', warrantyRequestsController.checkWarrantyRequests);
//warrantyRequestsRouter.get('/getWarrantyRequest', warrantyRequestsController.getWarrantyRequest);
warrantyRequestsRouter.get('/getWarrantyRequests', warrantyRequestsController.getWarrantyRequests);

warrantyRequestsRouter.get('/gerWarrantyRequestsById', warrantyRequestsController.getWarrantyRequestsById);
warrantyRequestsRouter.put('/updateWarrantyRequests', warrantyRequestsController.updateWarrantyRequests);
warrantyRequestsRouter.delete('/deleteWarrantyRequests', warrantyRequestsController.removeWarrantyRequests);
module.exports = warrantyRequestsRouter;