const express = require("express");
const customerRouter = express.Router();

const customerController = require('../controller/customer.controller');

customerRouter.post('/addCustomer', customerController.addCustomer);
customerRouter.get('/getCustomers', customerController.getCustomers);
customerRouter.get('/getCustomer', customerController.getCustomer)

module.exports = customerRouter;