const express = require("express");
const reportsRouter = express.Router();

const reportsController = require('../controller/reports.controller');

reportsRouter.post('/addCustomer', reportsController.addCustomer);
reportsRouter.get('/getSales', reportsController.getSales);
reportsRouter.get('/getCustomer', reportsController.getCustomer)

module.exports = reportsRouter;