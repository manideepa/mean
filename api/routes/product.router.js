const express = require("express");
const productRouter = express.Router();

const productController = require('../controller/product.controller');

productRouter.post('/addProduct', productController.addProduct);
productRouter.post('/checkProduct', productController.checkProduct);
productRouter.get('/getProducts', productController.getProducts);
productRouter.get('/getProductById', productController.getProductById);
productRouter.put('/updateProduct', productController.updateProduct);
productRouter.delete('/deleteProduct', productController.removeProduct);

module.exports = productRouter;