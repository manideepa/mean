const express = require("express");
const brandRouter = express.Router();

const brandController = require('../controller/brand.controller');

brandRouter.post('/addbrand', brandController.addBrand);
//productRouter.post('/checkProduct', brandController.checkProduct);
brandRouter.get('/getBrands', brandController.getBrands);
brandRouter.get('/getBrandById', brandController.getBrandById);
brandRouter.put('/updateBrand', brandController.updateBrand);
brandRouter.delete('/deleteBrand', brandController.removeBrand);

module.exports = brandRouter;