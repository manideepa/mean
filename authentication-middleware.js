const jwt = require("jsonwebtoken");

module.exports = () => {
    return (req, res, next) => {
        const token = req.headers['authorization'];
        if (!token) {
            res.status(401).send("Error,Access Denied")
        } else {
            const tokeyBody = token.slice(7)
            jwt.verify(tokeyBody, 'secret', (err, decoded) => {
                if (err) {
                    res.status(401).send("Error,Access Denied")
                }
                next();
            })

        }
    }
}