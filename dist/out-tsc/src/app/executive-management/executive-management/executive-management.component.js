import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ExecutiveServiceService } from '../../services/executive-service.service';
let ExecutiveManagementComponent = class ExecutiveManagementComponent {
    constructor(executiveService) {
        this.executiveService = executiveService;
    }
    ngOnInit() {
        this.executiveService.getExecutives().subscribe(data => {
            this.executivesList = data;
        });
    }
    deleteExecutive(id, i) {
        this.executivesList.splice(i, 1);
        this.executiveService.deleteExecutive(id).subscribe(data => {
            console.log(data);
            console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi", this.executivesList);
        });
    }
};
ExecutiveManagementComponent = tslib_1.__decorate([
    Component({
        selector: 'app-executive-management',
        templateUrl: './executive-management.component.html',
        styleUrls: ['./executive-management.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ExecutiveServiceService])
], ExecutiveManagementComponent);
export { ExecutiveManagementComponent };
//# sourceMappingURL=executive-management.component.js.map