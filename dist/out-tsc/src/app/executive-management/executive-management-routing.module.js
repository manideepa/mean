import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ExecutiveManagementComponent } from './executive-management/executive-management.component';
import { AddExecutiveComponent } from './add-executive/add-executive.component';
const routes = [
    { path: '', component: ExecutiveManagementComponent },
    { path: 'addexecutive', component: AddExecutiveComponent },
    { path: 'editExecutive/:id', component: AddExecutiveComponent }
];
let ExecutiveRoutingModule = class ExecutiveRoutingModule {
};
ExecutiveRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], ExecutiveRoutingModule);
export { ExecutiveRoutingModule };
//# sourceMappingURL=executive-management-routing.module.js.map