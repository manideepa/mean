import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RetailerManagementComponent } from './retailer-management/retailer-management.component';
import { AddRetailerComponent } from './add-retailer/add-retailer.component';
const routes = [
    { path: '', component: RetailerManagementComponent },
    { path: 'addretailer', component: AddRetailerComponent },
    { path: 'editRetailer/:id', component: AddRetailerComponent }
];
let RetailerRoutingModule = class RetailerRoutingModule {
};
RetailerRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], RetailerRoutingModule);
export { RetailerRoutingModule };
//# sourceMappingURL=retailer-management-routing.module.js.map