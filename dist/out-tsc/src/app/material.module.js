import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatRadioModule, MatFormFieldModule, MatInputModule, MatCardModule, MatIconModule, MatNativeDateModule, MatPaginatorModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTreeModule } from '@angular/material/tree';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatRippleModule } from '@angular/material/core';
let MaterialModule = class MaterialModule {
};
MaterialModule = tslib_1.__decorate([
    NgModule({
        imports: [
            MatButtonModule,
            MatCheckboxModule,
            MatRadioModule,
            MatFormFieldModule,
            MatInputModule,
            MatCardModule,
            MatIconModule,
            MatSidenavModule,
            MatTreeModule,
            MatDialogModule,
            MatDatepickerModule,
            MatTabsModule,
            MatTableModule,
            MatGridListModule,
            MatTooltipModule,
            MatNativeDateModule,
            MatSelectModule,
            MatPaginatorModule,
            MatSortModule,
            MatRippleModule
        ],
        exports: [
            MatButtonModule,
            MatCheckboxModule,
            MatRadioModule,
            MatFormFieldModule,
            MatInputModule,
            MatCardModule,
            MatIconModule,
            MatSidenavModule,
            MatTreeModule,
            MatDialogModule,
            MatDatepickerModule,
            MatTabsModule,
            MatTableModule,
            MatGridListModule,
            MatTooltipModule,
            MatSelectModule,
            MatPaginatorModule,
            MatSortModule,
            MatRippleModule
        ]
    })
], MaterialModule);
export { MaterialModule };
//# sourceMappingURL=material.module.js.map