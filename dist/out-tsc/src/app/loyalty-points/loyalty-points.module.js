import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLoyaltyPointsComponent } from './add-loyalty-points/add-loyalty-points.component';
import { LoyaltyPointsManagementComponent } from './loyalty-points-management/loyalty-points-management.component';
import { LoyaltyPointsRoutingModule } from './loyalty-points-routing.module';
import { MaterialModule } from './../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LoyaltyPointsServiceService } from './../services/loyalty-points-service.service';
import { HttpClientModule } from '@angular/common/http';
let LoyaltyPointsModule = class LoyaltyPointsModule {
};
LoyaltyPointsModule = tslib_1.__decorate([
    NgModule({
        declarations: [AddLoyaltyPointsComponent, LoyaltyPointsManagementComponent],
        imports: [
            CommonModule,
            LoyaltyPointsRoutingModule,
            MaterialModule,
            HttpClientModule,
            ReactiveFormsModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [LoyaltyPointsServiceService]
    })
], LoyaltyPointsModule);
export { LoyaltyPointsModule };
//# sourceMappingURL=loyalty-points.module.js.map