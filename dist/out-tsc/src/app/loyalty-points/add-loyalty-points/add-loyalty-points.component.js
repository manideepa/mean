import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { LoyaltyPointsServiceService } from "../../services/loyalty-points-service.service";
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
let AddLoyaltyPointsComponent = class AddLoyaltyPointsComponent {
    constructor(fb, loyaltyPointsService, router, sanitizer) {
        this.fb = fb;
        this.loyaltyPointsService = loyaltyPointsService;
        this.router = router;
        this.sanitizer = sanitizer;
        this.currentUrl = window.location.pathname.split("/");
    }
    ngOnInit() {
        this.loyaltyPointsForm = this.fb.group({
            id: [""],
            price: ["", Validators.required],
            points: ["", Validators.required]
        });
        if (this.currentUrl.includes("editloyalitypoints")) {
            let id = this.currentUrl[this.currentUrl.length - 1];
            console.log(id);
            this.loyaltyPointsService.getLoyaltyPointsById(id).subscribe(data => {
                let loyaltyPointsData = data;
                console.log(loyaltyPointsData);
                this.loyaltyPointsForm.patchValue({
                    id: loyaltyPointsData._id,
                    price: loyaltyPointsData.price,
                    points: loyaltyPointsData.points,
                });
            });
        }
    }
    addloyalitypoints() {
        if (this.currentUrl.includes("editloyalitypoints")) {
            console.log(this.loyaltyPointsForm.value);
            this.loyaltyPointsService.updateLoyaltyPoints(this.loyaltyPointsForm.value).subscribe(data => {
                this.router.navigate(["/loyalty-points"]);
            });
        }
        else {
            console.log(this.loyaltyPointsForm.value);
            this.loyaltyPointsService.addLoyaltyPoints(this.loyaltyPointsForm.value).subscribe(data => {
                console.log(data);
                this.router.navigate(["/loyalty-points"]);
            });
        }
    }
};
tslib_1.__decorate([
    ViewChild("fileInput", { static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], AddLoyaltyPointsComponent.prototype, "fileInput", void 0);
AddLoyaltyPointsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add-loyalty-points',
        templateUrl: './add-loyalty-points.component.html',
        styleUrls: ['./add-loyalty-points.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder,
        LoyaltyPointsServiceService,
        Router,
        DomSanitizer])
], AddLoyaltyPointsComponent);
export { AddLoyaltyPointsComponent };
//# sourceMappingURL=add-loyalty-points.component.js.map