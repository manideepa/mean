import { async, TestBed } from '@angular/core/testing';
import { AddLoyaltyPointsComponent } from './add-loyalty-points.component';
describe('AddLoyaltyPointsComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddLoyaltyPointsComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(AddLoyaltyPointsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=add-loyalty-points.component.spec.js.map