import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoyaltyPointsManagementComponent } from './loyalty-points-management/loyalty-points-management.component';
import { AddLoyaltyPointsComponent } from './add-loyalty-points/add-loyalty-points.component';
const routes = [
    { path: '', component: LoyaltyPointsManagementComponent },
    { path: 'addloyalitypoints', component: AddLoyaltyPointsComponent },
    { path: 'editloyalitypoints/:id', component: AddLoyaltyPointsComponent }
];
let LoyaltyPointsRoutingModule = class LoyaltyPointsRoutingModule {
};
LoyaltyPointsRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], LoyaltyPointsRoutingModule);
export { LoyaltyPointsRoutingModule };
//# sourceMappingURL=loyalty-points-routing.module.js.map