import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { LoyaltyPointsServiceService } from '../../services/loyalty-points-service.service';
let LoyaltyPointsManagementComponent = class LoyaltyPointsManagementComponent {
    constructor(loyaltyPointsServiceService) {
        this.loyaltyPointsServiceService = loyaltyPointsServiceService;
    }
    ngOnInit() {
        this.loyaltyPointsServiceService.getLoyaltyPoints().subscribe(data => {
            this.loyaltypointsList = data;
        });
    }
    deleteLoyaltyPoints(id, i) {
        this.loyaltypointsList.splice(i, 1);
        this.loyaltyPointsServiceService.deleteLoyaltyPoints(id).subscribe(data => {
            console.log(data);
            console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi", this.loyaltypointsList);
        });
    }
};
LoyaltyPointsManagementComponent = tslib_1.__decorate([
    Component({
        selector: 'app-loyalty-points-management',
        templateUrl: './loyalty-points-management.component.html',
        styleUrls: ['./loyalty-points-management.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [LoyaltyPointsServiceService])
], LoyaltyPointsManagementComponent);
export { LoyaltyPointsManagementComponent };
//# sourceMappingURL=loyalty-points-management.component.js.map