import { async, TestBed } from '@angular/core/testing';
import { LoyaltyPointsManagementComponent } from './loyalty-points-management.component';
describe('LoyaltyPointsManagementComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoyaltyPointsManagementComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(LoyaltyPointsManagementComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=loyalty-points-management.component.spec.js.map