import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserManagementComponent } from './user-management/user-management.component';
import { UserRoutingModule } from './user-management-routing.module';
import { MaterialModule } from './../material.module';
import { AddUserComponent } from './add-user/add-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserServiceService } from './../services/user-service.service';
import { HttpClientModule } from '@angular/common/http';
let UserManagementModule = class UserManagementModule {
};
UserManagementModule = tslib_1.__decorate([
    NgModule({
        declarations: [UserManagementComponent, AddUserComponent],
        imports: [
            CommonModule,
            UserRoutingModule,
            MaterialModule,
            HttpClientModule,
            ReactiveFormsModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [UserServiceService]
    })
], UserManagementModule);
export { UserManagementModule };
//# sourceMappingURL=user-management.module.js.map