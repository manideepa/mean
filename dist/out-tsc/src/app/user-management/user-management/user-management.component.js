import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserServiceService } from '../../services/user-service.service';
let UserManagementComponent = class UserManagementComponent {
    constructor(userService) {
        this.userService = userService;
    }
    ngOnInit() {
        this.userService.getUsers().subscribe(data => {
            this.usersList = data;
        });
    }
    deleteUser(id, i) {
        this.usersList.splice(i, 1);
        this.userService.deleteUser(id).subscribe(data => {
            console.log(data);
            console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi", this.usersList);
        });
    }
};
UserManagementComponent = tslib_1.__decorate([
    Component({
        selector: 'app-user-management',
        templateUrl: './user-management.component.html',
        styleUrls: ['./user-management.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [UserServiceService])
], UserManagementComponent);
export { UserManagementComponent };
//# sourceMappingURL=user-management.component.js.map