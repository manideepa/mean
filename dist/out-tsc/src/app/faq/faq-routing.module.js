import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FaqManagementComponent } from './faq-management/faq-management.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
const routes = [
    { path: '', component: FaqManagementComponent },
    { path: 'addfaq', component: AddFaqComponent },
    { path: 'editFaq/:id', component: AddFaqComponent }
];
let FaqRoutingModule = class FaqRoutingModule {
};
FaqRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], FaqRoutingModule);
export { FaqRoutingModule };
//# sourceMappingURL=faq-routing.module.js.map