import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { FaqManagementComponent } from './faq-management/faq-management.component';
import { FaqRoutingModule } from './faq-routing.module';
import { MaterialModule } from './../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FaqServiceService } from './../services/faq-service.service';
import { HttpClientModule } from '@angular/common/http';
let FaqModule = class FaqModule {
};
FaqModule = tslib_1.__decorate([
    NgModule({
        declarations: [AddFaqComponent, FaqManagementComponent],
        imports: [
            CommonModule,
            FaqRoutingModule,
            MaterialModule,
            HttpClientModule,
            ReactiveFormsModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [FaqServiceService]
    })
], FaqModule);
export { FaqModule };
//# sourceMappingURL=faq.module.js.map