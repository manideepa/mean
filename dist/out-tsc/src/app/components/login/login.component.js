import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { UserServiceService } from '../../services/user-service.service';
import { Router } from "@angular/router";
let LoginComponent = class LoginComponent {
    constructor(fb, userService, router) {
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.isAuthError = false;
    }
    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    login() {
        this.isAuthError = false;
        this.userService.checkUser(this.loginForm.value).subscribe((data) => {
            console.log(data);
            localStorage.setItem('token', data);
            this.router.navigate(['']);
        }, (errror) => {
            this.isAuthError = true;
        });
    }
};
LoginComponent = tslib_1.__decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder, UserServiceService, Router])
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map