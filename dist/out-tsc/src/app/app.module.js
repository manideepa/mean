import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserServiceService } from './services/user-service.service';
import { RetailerServiceService } from './services/retailer-service.service';
import { ProductServiceService } from './services/product-service.service';
import { BrandServiceService } from './services/brand-service.service';
import { SalesServiceService } from './services/sales-service.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent,
            LoginComponent
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            BrowserAnimationsModule,
            HttpClientModule,
            ReactiveFormsModule,
            MaterialModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [UserServiceService, RetailerServiceService, ProductServiceService, BrandServiceService, SalesServiceService],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map