import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { BrandServiceService } from "../../services/brand-service.service";
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
let AddBrandComponent = class AddBrandComponent {
    constructor(fb, brandService, router, sanitizer) {
        this.fb = fb;
        this.brandService = brandService;
        this.router = router;
        this.sanitizer = sanitizer;
        this.currentUrl = window.location.pathname.split("/");
    }
    ngOnInit() {
        this.brandForm = this.fb.group({
            id: [""],
            brand: ["", Validators.required],
            imgData: [],
            created_at: Date(),
            updated_at: []
        });
        if (this.currentUrl.includes("editBrand")) {
            let id = this.currentUrl[this.currentUrl.length - 1];
            console.log(id);
            this.brandService.getBrandById(id).subscribe(data => {
                let brandData = data;
                console.log(brandData);
                this.imageData = brandData.imgData;
                this.brandForm.patchValue({
                    id: brandData._id,
                    brand: brandData.brand,
                    imgData: this.brandImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + brandData.imgData),
                    updated_at: new Date()
                });
            });
        }
    }
    addBrand() {
        if (this.currentUrl.includes("editBrand")) {
            console.log(this.brandForm.value);
            this.brandService.updateBrand(this.brandForm.value).subscribe(data => {
                this.router.navigate(["/brand"]);
            });
        }
        else {
            this.brandService.addBrand(this.brandForm.value).subscribe(data => {
                console.log(data);
                this.router.navigate(["/brand"]);
            });
        }
    }
    onFileInput(e) {
        let file = e.target.files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = this.handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }
    handleReaderLoaded(readerEvt) {
        let binaryString = readerEvt.target.result;
        this.base64textString = btoa(binaryString);
        let imgBaseCode = btoa(binaryString);
        this.brandForm.patchValue({
            imgData: imgBaseCode
        });
        this.brandImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + imgBaseCode);
    }
    deleteImg() {
        this.brandForm.get("imgData").setValue("");
        this.brandImg = "";
        this.fileInput.nativeElement.value = "";
    }
};
tslib_1.__decorate([
    ViewChild("fileInput", { static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], AddBrandComponent.prototype, "fileInput", void 0);
AddBrandComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add-brand',
        templateUrl: './add-brand.component.html',
        styleUrls: ['./add-brand.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder,
        BrandServiceService,
        Router,
        DomSanitizer])
], AddBrandComponent);
export { AddBrandComponent };
//# sourceMappingURL=add-brand.component.js.map