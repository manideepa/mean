import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBrandComponent } from './add-brand/add-brand.component';
import { BrandManagementComponent } from './brand-management/brand-management.component';
import { BrandRoutingModule } from './brand-management-routing.module';
import { MaterialModule } from './../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BrandServiceService } from './../services/brand-service.service';
import { HttpClientModule } from '@angular/common/http';
let BrandManagementModule = class BrandManagementModule {
};
BrandManagementModule = tslib_1.__decorate([
    NgModule({
        declarations: [AddBrandComponent, BrandManagementComponent],
        imports: [
            CommonModule,
            BrandRoutingModule,
            MaterialModule,
            HttpClientModule,
            ReactiveFormsModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [BrandServiceService]
    })
], BrandManagementModule);
export { BrandManagementModule };
//# sourceMappingURL=brand-management.module.js.map