import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BrandServiceService } from '../../services/brand-service.service';
let BrandManagementComponent = class BrandManagementComponent {
    constructor(brandService) {
        this.brandService = brandService;
    }
    ngOnInit() {
        this.brandService.getBrands().subscribe(data => {
            this.brandsList = data;
        });
    }
    deleteBrand(id, i) {
        this.brandsList.splice(i, 1);
        this.brandService.deleteBrand(id).subscribe(data => {
            console.log(data);
            console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi", this.brandsList);
        });
    }
};
BrandManagementComponent = tslib_1.__decorate([
    Component({
        selector: 'app-brand-management',
        templateUrl: './brand-management.component.html',
        styleUrls: ['./brand-management.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [BrandServiceService])
], BrandManagementComponent);
export { BrandManagementComponent };
//# sourceMappingURL=brand-management.component.js.map