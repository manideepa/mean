import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrandManagementComponent } from './brand-management/brand-management.component';
import { AddBrandComponent } from './add-brand/add-brand.component';
const routes = [
    { path: '', component: BrandManagementComponent },
    { path: 'addBrand', component: AddBrandComponent },
    { path: 'editBrand/:id', component: AddBrandComponent }
];
let BrandRoutingModule = class BrandRoutingModule {
};
BrandRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], BrandRoutingModule);
export { BrandRoutingModule };
//# sourceMappingURL=brand-management-routing.module.js.map