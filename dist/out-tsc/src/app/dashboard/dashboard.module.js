import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MaterialModule } from './../material.module';
import { ClickOutsideModule } from 'ng-click-outside';
import { HighchartsChartModule } from 'highcharts-angular';
let DashboardModule = class DashboardModule {
};
DashboardModule = tslib_1.__decorate([
    NgModule({
        declarations: [DashboardComponent],
        imports: [
            CommonModule,
            DashboardRoutingModule,
            MaterialModule,
            ClickOutsideModule,
            HighchartsChartModule
        ],
        exports: [
            MaterialModule
        ]
    })
], DashboardModule);
export { DashboardModule };
//# sourceMappingURL=dashboard.module.js.map