import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import * as Highcharts from 'highcharts';
import { UserServiceService } from '../../services/user-service.service';
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
const ELEMENT_DATA = [
    { position: 1, orderId: 'Hydrogen', item: "Tv", status: 'H' },
    { position: 2, orderId: 'Helium', item: "Tv", status: 'He' },
    { position: 3, orderId: 'Lithium', item: "Tv", status: 'Li' },
    { position: 4, orderId: 'Beryllium', item: "Tv", status: 'Be' },
    { position: 5, orderId: 'Boron', item: "Tv", status: 'B' },
    { position: 6, orderId: 'Carbon', item: "Tv", status: 'C' }
];
let DashboardComponent = class DashboardComponent {
    constructor(userService, router, sanitizer) {
        this.userService = userService;
        this.router = router;
        this.sanitizer = sanitizer;
        this.isAdmin = false;
        this.isExecutive = false;
        this.showDashboard = false;
        this.showUsermenu = false;
        this.displayedColumns = ['position', 'orderId', 'item', 'status'];
        this.dataSource = ELEMENT_DATA;
        this.highcharts = Highcharts;
        this.chartOptions = {
            chart: {
                type: "spline"
            },
            title: {
                text: "Monthly Sales"
            },
            subtitle: {
                text: "WarrantyME"
            },
            xAxis: {
                categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            },
            yAxis: {
                title: {
                    text: "Sales"
                }
            },
            tooltip: {
                valueSuffix: "Number"
            },
            series: [
                {
                    name: 'CHROMA',
                    data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
                },
                {
                    name: 'RELAINCE DIGITAL',
                    data: [0, 0, 5, 11, 17, 22, 24, 24, 20, 14, 8, 2]
                },
                {
                    name: 'BIG C',
                    data: [0, 0, 3, 8, 13, 17, 18, 17, 14, 9, 3, 1]
                },
                {
                    name: 'REDMI',
                    data: [3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
                }
            ]
        };
        this.chartOptions1 = {
            chart: {
                type: "scatter"
            },
            title: {
                text: "Monthly Retailers"
            },
            subtitle: {
                text: "WarrantyME"
            },
            xAxis: {
                categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            },
            yAxis: {
                title: {
                    text: "Sales"
                }
            },
            tooltip: {
                valueSuffix: "Number"
            },
            series: [
                {
                    name: 'CHANDANAGAR',
                    data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
                },
                {
                    name: 'KPHB',
                    data: [0, 0, 5, 11, 17, 22, 24, 24, 20, 14, 8, 2]
                },
                {
                    name: 'MADHAPUR',
                    data: [0, 0, 3, 8, 13, 17, 18, 17, 14, 9, 3, 1]
                },
                {
                    name: 'GAUCHIBOWLI',
                    data: [3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
                }
            ]
        };
        this.router.events.subscribe(event => {
            if (event) {
                if (this.router.url == "/") {
                    this.showDashboard = true;
                }
                else {
                    this.showDashboard = false;
                }
            }
        });
    }
    ngOnInit() {
        let token = localStorage.getItem('token');
        if (token) {
            this.userService.getUserInfo(token).subscribe(data => {
                this.userData = data;
                this.isAdmin = this.userData.roles.includes('admin') ? true : false;
                this.isExecutive = (this.userData.roles.includes('retailer') || this.userData.roles.includes('admin')) ? true : false;
                console.log(this.isExecutive);
                this.userService.getUserById(this.userData.id).subscribe((data) => {
                    this.singleUser = data;
                    this.imageData = this.singleUser.imgData;
                    this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + this.singleUser.imgData);
                });
            }, (error) => {
                this.router.navigate(['/login']);
            });
        }
        else {
            this.router.navigate(['/login']);
        }
    }
    showMenu() {
        this.showUsermenu = !this.showUsermenu;
    }
    logout() {
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
    }
};
DashboardComponent = tslib_1.__decorate([
    Component({
        selector: 'app-dashboard',
        templateUrl: './dashboard.component.html',
        styleUrls: ['./dashboard.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [UserServiceService, Router, DomSanitizer])
], DashboardComponent);
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map