import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
const routes = [
    { path: '', component: DashboardComponent,
        children: [{
                path: 'user-management', loadChildren: '../user-management/user-management.module#UserManagementModule'
            }, {
                path: 'customer-management', loadChildren: '../customer/customer.module#CustomerModule'
            }, {
                path: 'retailer', loadChildren: '../retailer-management/retailer-management.module#RetailerManagementModule'
            }, {
                path: 'brand', loadChildren: '../brand-management/brand-management.module#BrandManagementModule'
            }, {
                path: 'executive-management', loadChildren: '../executive-management/executive-management.module#ExecutiveManagementModule'
            }, {
                path: 'product', loadChildren: '../product-management/product-management.module#ProductManagementModule'
            }, {
                path: 'sales', loadChildren: '../sales/sales.module#SalesModule'
            }, {
                path: 'loyalty-points', loadChildren: '../loyalty-points/loyalty-points.module#LoyaltyPointsModule'
            }, {
                path: 'faq', loadChildren: '../faq/faq.module#FaqModule'
            }]
    }
];
let DashboardRoutingModule = class DashboardRoutingModule {
};
DashboardRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], DashboardRoutingModule);
export { DashboardRoutingModule };
//# sourceMappingURL=dashboard-routing.module.js.map