import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SalesManagementComponent } from './sales-management/sales-management.component';
import { AddSalesComponent } from './add-sales/add-sales.component';
const routes = [
    { path: '', component: SalesManagementComponent },
    { path: 'addsales', component: AddSalesComponent },
    { path: 'editSales/:id', component: AddSalesComponent }
];
let SalesRoutingModule = class SalesRoutingModule {
};
SalesRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], SalesRoutingModule);
export { SalesRoutingModule };
//# sourceMappingURL=sales-routing.module.js.map