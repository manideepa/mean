import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { SalesServiceService } from "../../services/sales-service.service";
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
let AddSalesComponent = class AddSalesComponent {
    constructor(fb, salesService, router, sanitizer) {
        this.fb = fb;
        this.salesService = salesService;
        this.router = router;
        this.sanitizer = sanitizer;
        this.currentUrl = window.location.pathname.split("/");
    }
    ngOnInit() {
        this.salesForm = this.fb.group({
            id: [""],
            model: ["", Validators.required],
            brand: ["", Validators.required],
            model_number: ["", Validators.required],
            price: ["", Validators.required],
            date_of_purchase: ["", Validators.required],
            period_of_warranty: ["", Validators.required],
            retailer_product_id: ["", Validators.required],
            product_type: ["", Validators.required],
            serial_number: ["", Validators.required],
            IMEI: ["", Validators.required],
            imgData: []
        });
        if (this.currentUrl.includes("editSales")) {
            let id = this.currentUrl[this.currentUrl.length - 1];
            console.log(id);
            this.salesService.getSalesById(id).subscribe(data => {
                let salesData = data;
                console.log(salesData);
                this.imageData = salesData.imgData;
                this.salesForm.patchValue({
                    id: salesData._id,
                    model: salesData.model,
                    brand: salesData.brand,
                    model_number: salesData.model_number,
                    price: salesData.price,
                    date_of_purchase: salesData.date_of_purchase,
                    period_of_warranty: salesData.period_of_warranty,
                    retailer_product_id: salesData.retailer_product_id,
                    product_type: salesData.product_type,
                    serial_number: salesData.serial_number,
                    IMEI: salesData.IMEI,
                    imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + salesData.imgData)
                });
            });
        }
    }
    addSales() {
        if (this.currentUrl.includes("editSales")) {
            console.log(this.salesForm.value);
            this.salesService.updateSales(this.salesForm.value).subscribe(data => {
                this.router.navigate(["/sales"]);
            });
        }
        else {
            console.log(this.salesForm.value);
            this.salesService.addSales(this.salesForm.value).subscribe(data => {
                console.log(data);
                this.router.navigate(["/sales"]);
            });
        }
    }
    onFileInput(e) {
        let file = e.target.files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = this.handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }
    handleReaderLoaded(readerEvt) {
        let binaryString = readerEvt.target.result;
        this.base64textString = btoa(binaryString);
        let imgBaseCode = btoa(binaryString);
        this.salesForm.patchValue({
            imgData: imgBaseCode
        });
        this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + imgBaseCode);
    }
    deleteImg() {
        this.salesForm.get("imgData").setValue("");
        this.userImg = "";
        this.fileInput.nativeElement.value = "";
    }
};
tslib_1.__decorate([
    ViewChild("fileInput", { static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], AddSalesComponent.prototype, "fileInput", void 0);
AddSalesComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add-sales',
        templateUrl: './add-sales.component.html',
        styleUrls: ['./add-sales.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder,
        SalesServiceService,
        Router,
        DomSanitizer])
], AddSalesComponent);
export { AddSalesComponent };
//# sourceMappingURL=add-sales.component.js.map