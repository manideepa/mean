import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { SalesServiceService } from '../../services/sales-service.service';
let SalesManagementComponent = class SalesManagementComponent {
    constructor(salesService) {
        this.salesService = salesService;
    }
    ngOnInit() {
        this.salesService.getSales().subscribe(data => {
            this.salesList = data;
        });
    }
    deleteSales(id, i) {
        this.salesList.splice(i, 1);
        this.salesService.deleteSales(id).subscribe(data => {
            console.log(data);
            console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi", this.salesList);
        });
    }
};
SalesManagementComponent = tslib_1.__decorate([
    Component({
        selector: 'app-sales-management',
        templateUrl: './sales-management.component.html',
        styleUrls: ['./sales-management.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [SalesServiceService])
], SalesManagementComponent);
export { SalesManagementComponent };
//# sourceMappingURL=sales-management.component.js.map