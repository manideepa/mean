import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
import { CustomerRoutingModule } from './customer-routing.module';
import { MaterialModule } from './../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomerServiceService } from './../services/customer-service.service';
import { HttpClientModule } from '@angular/common/http';
let CustomerModule = class CustomerModule {
};
CustomerModule = tslib_1.__decorate([
    NgModule({
        declarations: [CustomerManagementComponent],
        imports: [
            CommonModule,
            CustomerRoutingModule,
            MaterialModule,
            HttpClientModule,
            ReactiveFormsModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [CustomerServiceService]
    })
], CustomerModule);
export { CustomerModule };
//# sourceMappingURL=customer.module.js.map