import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
//import { AddFaqComponent } from './add-faq/add-faq.component';
const routes = [
    { path: '', component: CustomerManagementComponent },
];
let CustomerRoutingModule = class CustomerRoutingModule {
};
CustomerRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], CustomerRoutingModule);
export { CustomerRoutingModule };
//# sourceMappingURL=customer-routing.module.js.map