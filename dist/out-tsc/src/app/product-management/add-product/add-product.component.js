import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { ProductServiceService } from "../../services/product-service.service";
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
let AddProductComponent = class AddProductComponent {
    constructor(fb, productService, router, sanitizer) {
        this.fb = fb;
        this.productService = productService;
        this.router = router;
        this.sanitizer = sanitizer;
        this.currentUrl = window.location.pathname.split("/");
    }
    ngOnInit() {
        this.productForm = this.fb.group({
            id: [""],
            model: ["", Validators.required],
            brand: ["", Validators.required],
            model_number: ["", Validators.required],
            price: ["", Validators.required],
            date_of_purchase: ["", Validators.required],
            period_of_warranty: ["", Validators.required],
            retailer_product_id: ["", Validators.required],
            product_type: ["", Validators.required],
            serial_number: ["", Validators.required],
            IMEI: ["", Validators.required],
            imgData: []
        });
        if (this.currentUrl.includes("editProduct")) {
            let id = this.currentUrl[this.currentUrl.length - 1];
            console.log(id);
            this.productService.getProductById(id).subscribe(data => {
                let productData = data;
                console.log(productData);
                this.imageData = productData.imgData;
                this.productForm.patchValue({
                    id: productData._id,
                    model: productData.model,
                    brand: productData.brand,
                    model_number: productData.model_number,
                    price: productData.price,
                    date_of_purchase: productData.date_of_purchase,
                    period_of_warranty: productData.period_of_warranty,
                    retailer_product_id: productData.retailer_product_id,
                    product_type: productData.product_type,
                    serial_number: productData.serial_number,
                    IMEI: productData.IMEI,
                    imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + productData.imgData)
                });
            });
        }
    }
    addProduct() {
        if (this.currentUrl.includes("editProduct")) {
            console.log(this.productForm.value);
            this.productService.updateProduct(this.productForm.value).subscribe(data => {
                this.router.navigate(["/product"]);
            });
        }
        else {
            console.log(this.productForm.value);
            this.productService.addProduct(this.productForm.value).subscribe(data => {
                console.log(data);
                this.router.navigate(["/product"]);
            });
        }
    }
    onFileInput(e) {
        let file = e.target.files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = this.handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }
    handleReaderLoaded(readerEvt) {
        let binaryString = readerEvt.target.result;
        this.base64textString = btoa(binaryString);
        let imgBaseCode = btoa(binaryString);
        this.productForm.patchValue({
            imgData: imgBaseCode
        });
        this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + imgBaseCode);
    }
    deleteImg() {
        this.productForm.get("imgData").setValue("");
        this.userImg = "";
        this.fileInput.nativeElement.value = "";
    }
};
tslib_1.__decorate([
    ViewChild("fileInput", { static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], AddProductComponent.prototype, "fileInput", void 0);
AddProductComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add-product',
        templateUrl: './add-product.component.html',
        styleUrls: ['./add-product.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder,
        ProductServiceService,
        Router,
        DomSanitizer])
], AddProductComponent);
export { AddProductComponent };
//# sourceMappingURL=add-product.component.js.map