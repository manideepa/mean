import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductManagementComponent } from './product-management/product-management.component';
import { ProductRoutingModule } from './product-management-routing.module';
import { MaterialModule } from './../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductServiceService } from './../services/product-service.service';
import { HttpClientModule } from '@angular/common/http';
let ProductManagementModule = class ProductManagementModule {
};
ProductManagementModule = tslib_1.__decorate([
    NgModule({
        declarations: [AddProductComponent, ProductManagementComponent],
        imports: [
            CommonModule,
            ProductRoutingModule,
            MaterialModule,
            HttpClientModule,
            ReactiveFormsModule
        ],
        exports: [
            MaterialModule
        ],
        providers: [ProductServiceService]
    })
], ProductManagementModule);
export { ProductManagementModule };
//# sourceMappingURL=product-management.module.js.map