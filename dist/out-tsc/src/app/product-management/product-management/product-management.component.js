import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ProductServiceService } from '../../services/product-service.service';
let ProductManagementComponent = class ProductManagementComponent {
    constructor(productService) {
        this.productService = productService;
    }
    ngOnInit() {
        this.productService.getProducts().subscribe(data => {
            this.productsList = data;
        });
    }
    deleteProduct(id, i) {
        this.productsList.splice(i, 1);
        this.productService.deleteProduct(id).subscribe(data => {
            console.log(data);
            console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi", this.productsList);
        });
    }
};
ProductManagementComponent = tslib_1.__decorate([
    Component({
        selector: 'app-product-management',
        templateUrl: './product-management.component.html',
        styleUrls: ['./product-management.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ProductServiceService])
], ProductManagementComponent);
export { ProductManagementComponent };
//# sourceMappingURL=product-management.component.js.map