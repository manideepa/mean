import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductManagementComponent } from './product-management/product-management.component';
import { AddProductComponent } from './add-product/add-product.component';
const routes = [
    { path: '', component: ProductManagementComponent },
    { path: 'addproduct', component: AddProductComponent },
    { path: 'editProduct/:id', component: AddProductComponent }
];
let ProductRoutingModule = class ProductRoutingModule {
};
ProductRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], ProductRoutingModule);
export { ProductRoutingModule };
//# sourceMappingURL=product-management-routing.module.js.map