import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let RetailerServiceService = class RetailerServiceService {
    constructor(http) {
        this.http = http;
    }
    addRetailer(retailerObject) {
        return this.http.post('http://localhost:3030/retailers/addRetailer', retailerObject);
    }
    updateRetailer(retailerObject) {
        return this.http.put('http://localhost:3030/retailers/updateRetailer', retailerObject);
    }
    checkRetailer(loginObject) {
        return this.http.post('http://localhost:3030/retailers/checkretailer', loginObject);
    }
    getRetailerInfo(token) {
        return this.http.get('http://localhost:3030/retailers/getRetailer?token=' + token);
    }
    deleteRetailer(retailerId) {
        return this.http.delete('http://localhost:3030/retailers/deleteRetailer?id=' + retailerId);
    }
    getRetailers() {
        return this.http.get('http://localhost:3030/retailers/getRetailers');
    }
    getRetailerById(id) {
        return this.http.get('http://localhost:3030/retailers/gerRetailerById?id=' + id);
    }
};
RetailerServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], RetailerServiceService);
export { RetailerServiceService };
//# sourceMappingURL=retailer-service.service.js.map