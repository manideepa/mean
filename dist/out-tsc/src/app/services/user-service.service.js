import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
let UserServiceService = class UserServiceService {
    constructor(http) {
        this.http = http;
    }
    addUser(userObject) {
        return this.http.post('http://localhost:3030/users/addUser', userObject);
    }
    updateUser(userObject) {
        return this.http.put('http://localhost:3030/users/updateUser', userObject);
    }
    checkUser(loginObject) {
        return this.http.post(`${environment.baseUrl}users/checkuser`, loginObject);
    }
    getUserInfo(token) {
        return this.http.get('http://localhost:3030/users/getUser?token=' + token);
    }
    deleteUser(userId) {
        return this.http.delete('http://localhost:3030/users/deleteUser?id=' + userId);
    }
    getUsers() {
        return this.http.get('http://localhost:3030/users/getUsers');
    }
    getRetailers() {
        return this.http.get('http://localhost:3030/users/getRetailers');
    }
    getUserById(id) {
        return this.http.get('http://localhost:3030/users/gerUserById?id=' + id);
    }
};
UserServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], UserServiceService);
export { UserServiceService };
//# sourceMappingURL=user-service.service.js.map