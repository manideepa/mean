import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let BrandServiceService = class BrandServiceService {
    constructor(http) {
        this.http = http;
    }
    addBrand(brandObject) {
        return this.http.post('http://localhost:3030/brands/addBrand', brandObject);
    }
    updateBrand(brandObject) {
        return this.http.put('http://localhost:3030/brands/updateBrand', brandObject);
    }
    checkBrand(loginObject) {
        return this.http.post('http://localhost:3030/brands/checkbrand', loginObject);
    }
    getBrandInfo(token) {
        return this.http.get('http://localhost:3030/brands/getBrand?token=' + token);
    }
    deleteBrand(brandId) {
        return this.http.delete('http://localhost:3030/brands/deleteBrand?id=' + brandId);
    }
    getBrands() {
        return this.http.get('http://localhost:3030/brands/getBrands');
    }
    getBrandById(id) {
        return this.http.get('http://localhost:3030/brands/getBrandById?id=' + id);
    }
};
BrandServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], BrandServiceService);
export { BrandServiceService };
//# sourceMappingURL=brand-service.service.js.map