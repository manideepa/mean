import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let ExecutiveServiceService = class ExecutiveServiceService {
    constructor(http) {
        this.http = http;
    }
    addExecutive(executiveObject) {
        return this.http.post('http://localhost:3030/executives/addExecutive', executiveObject);
    }
    updateExecutive(executiveObject) {
        return this.http.put('http://localhost:3030/executives/updateExecutive', executiveObject);
    }
    checkExecutive(loginObject) {
        return this.http.post('http://localhost:3030/executives/checkexecutive', loginObject);
    }
    getExecutiveInfo(token) {
        return this.http.get('http://localhost:3030/executives/getExecutive?token=' + token);
    }
    deleteExecutive(executiveId) {
        return this.http.delete('http://localhost:3030/executives/deleteExecutive?id=' + executiveId);
    }
    getExecutives() {
        return this.http.get('http://localhost:3030/executives/getExecutives');
    }
    getExecutiveById(id) {
        return this.http.get('http://localhost:3030/executives/gerExecutiveById?id=' + id);
    }
};
ExecutiveServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], ExecutiveServiceService);
export { ExecutiveServiceService };
//# sourceMappingURL=executive-service.service.js.map