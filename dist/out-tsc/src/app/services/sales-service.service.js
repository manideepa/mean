import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let SalesServiceService = class SalesServiceService {
    constructor(http) {
        this.http = http;
    }
    addSales(salesObject) {
        console.log(salesObject);
        return this.http.post('http://localhost:3030/sales/addSales', salesObject);
    }
    updateSales(salesObject) {
        console.log(salesObject);
        return this.http.put('http://localhost:3030/sales/updateSales', salesObject);
    }
    checkSales(loginObject) {
        return this.http.post('http://localhost:3030/sales/checksales', loginObject);
    }
    getSalesInfo(token) {
        return this.http.get('http://localhost:3030/sales/getSales?token=' + token);
    }
    deleteSales(salesId) {
        return this.http.delete('http://localhost:3030/sales/deleteSales?id=' + salesId);
    }
    getSales() {
        return this.http.get('http://localhost:3030/sales/getSales');
    }
    getSalesById(id) {
        return this.http.get('http://localhost:3030/sales/getSalesById?id=' + id);
    }
};
SalesServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], SalesServiceService);
export { SalesServiceService };
//# sourceMappingURL=sales-service.service.js.map