import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let FaqServiceService = class FaqServiceService {
    constructor(http) {
        this.http = http;
    }
    addFaq(faqObject) {
        console.log(faqObject);
        return this.http.post('http://localhost:3030/faq/addFaq', faqObject);
    }
    updateFaq(faqObject) {
        console.log(faqObject);
        return this.http.put('http://localhost:3030/faq/updateFaq', faqObject);
    }
    checkFaq(loginObject) {
        return this.http.post('http://localhost:3030/sales/checkfaq', loginObject);
    }
    getFaqInfo(token) {
        return this.http.get('http://localhost:3030/sales/getFaq?token=' + token);
    }
    deleteFaq(faqId) {
        return this.http.delete('http://localhost:3030/sales/deleteFaq?id=' + faqId);
    }
    getFaq() {
        return this.http.get('http://localhost:3030/sales/getFaq');
    }
    getFaqById(id) {
        return this.http.get('http://localhost:3030/sales/getFaqById?id=' + id);
    }
};
FaqServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], FaqServiceService);
export { FaqServiceService };
//# sourceMappingURL=faq-service.service.js.map