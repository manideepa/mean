import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
let CustomerServiceService = class CustomerServiceService {
    constructor(http) {
        this.http = http;
    }
    getCustomer() {
        return this.http.get(`${environment.baseUrl}customer/getCustomer`);
    }
    getCustomerById(id) {
        return this.http.get(`${environment.baseUrl}getCustomerById?id=${id}`);
    }
};
CustomerServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], CustomerServiceService);
export { CustomerServiceService };
//# sourceMappingURL=customer-service.service.js.map