import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let LoyaltyPointsServiceService = class LoyaltyPointsServiceService {
    constructor(http) {
        this.http = http;
    }
    addLoyaltyPoints(loyaltyPointsObject) {
        console.log(loyaltyPointsObject);
        return this.http.post('http://localhost:3030/loyalty-points/addLoyaltyPoints', loyaltyPointsObject);
    }
    updateLoyaltyPoints(loyaltyPointsObject) {
        console.log(loyaltyPointsObject);
        return this.http.put('http://localhost:3030/loyalty-points/updateLoyaltyPoints', loyaltyPointsObject);
    }
    checkLoyaltyPoints(loginObject) {
        return this.http.post('http://localhost:3030/loyalty-points/checkLoyaltyPoints', loginObject);
    }
    deleteLoyaltyPoints(loyaltyPointsId) {
        return this.http.delete('http://localhost:3030/loyalty-points/deleteLoyaltyPoints?id=' + loyaltyPointsId);
    }
    getLoyaltyPoints() {
        return this.http.get('http://localhost:3030/loyalty-points/getLoyaltyPoints');
    }
    getLoyaltyPointsById(id) {
        return this.http.get('http://localhost:3030/loyalty-points/getLoyaltyPointsById?id=' + id);
    }
};
LoyaltyPointsServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], LoyaltyPointsServiceService);
export { LoyaltyPointsServiceService };
//# sourceMappingURL=loyalty-points-service.service.js.map