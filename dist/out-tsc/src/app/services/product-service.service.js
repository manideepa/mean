import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
let ProductServiceService = class ProductServiceService {
    constructor(http) {
        this.http = http;
    }
    addProduct(productObject) {
        console.log(productObject);
        return this.http.post('http://localhost:3030/products/addProduct', productObject);
    }
    updateProduct(productObject) {
        console.log(productObject);
        return this.http.put('http://localhost:3030/products/updateProduct', productObject);
    }
    checkProduct(loginObject) {
        return this.http.post('http://localhost:3030/products/checkproduct', loginObject);
    }
    getProductInfo(token) {
        return this.http.get('http://localhost:3030/products/getProduct?token=' + token);
    }
    deleteProduct(productId) {
        return this.http.delete('http://localhost:3030/products/deleteProduct?id=' + productId);
    }
    getProducts() {
        return this.http.get('http://localhost:3030/products/getProducts');
    }
    getProductById(id) {
        return this.http.get('http://localhost:3030/products/getProductById?id=' + id);
    }
};
ProductServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], ProductServiceService);
export { ProductServiceService };
//# sourceMappingURL=product-service.service.js.map