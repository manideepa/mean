import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductManagementComponent } from './product-management/product-management.component';
import { AddProductComponent } from './add-product/add-product.component';


const routes: Routes = [
    {path: '', component: ProductManagementComponent},
    {path: 'addproduct',component:AddProductComponent},
    {path: 'editProduct/:id',component: AddProductComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
