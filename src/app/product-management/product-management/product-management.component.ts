import { Component, OnInit, ViewChild} from '@angular/core';
import { ProductServiceService }  from '../../services/product-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-product-management',
  templateUrl: './product-management.component.html',
  styleUrls: ['./product-management.component.scss']
})
export class ProductManagementComponent implements OnInit {
  products: [];
  productsList: [];
  displayedColumns = ['sno','model','brand','model_number','price','action'];
  dataSource: MatTableDataSource<productsList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private productService: ProductServiceService, private router: Router) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(data => {
      this.products = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  deleteProduct(id,i) {
    this.products.splice(i,1)
    this.productService.deleteProduct(id).subscribe(data => {
      console.log(data);
      location.reload();
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.productsList)
    })
  }

}

export interface productsList {
  sno:string;
  model: string;
  brand: string;
  model_number: string;
  price: string;
  action: string;
}