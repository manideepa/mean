import { Component, OnInit,ViewChild,  ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { ProductServiceService } from "../../services/product-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { BrandServiceService }  from '../../services/brand-service.service';
import { CategoryServiceService }  from '../../services/category-service.service';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
			@ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  productForm: FormGroup;
  brand: [];
  category: [];
  currentUrl = window.location.pathname.split("/");
  base64textString;
  userImg;
 imageData
  constructor(
  	private fb: FormBuilder,
    private productService: ProductServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private brandService:BrandServiceService,
    private categoryService:CategoryServiceService,
  ) {
    this.productForm = this.fb.group({
      brand: [''],
      category : ['']
    });
  }

  ngOnInit() {

  this.productForm = this.fb.group({
      id: [""],
      model: ["", Validators.required],
      brand: ["", Validators.required],
      category: ["", Validators.required],
      model_number: ["", Validators.required],
      price: ["", Validators.required],
	  date_of_purchase: ["", Validators.required],
	  period_of_warranty: ["", Validators.required],
	  retailer_product_id: ["", Validators.required],
	  product_type: ["", Validators.required],
	  serial_number: ["", Validators.required],
	  IMEI: ["", Validators.required],
      imgData: []
    });

    this.getBrands();
    this.getCategories();

    if (this.currentUrl.includes("editProduct")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.productService.getProductById(id).subscribe(data => {
        let productData = data;
        console.log(productData);
        this.imageData = productData.imgData;
        this.productForm.patchValue({
          id: productData._id,
          model: productData.model,
          brand: productData.brand,
          category: productData.category,
          model_number: productData.model_number,
          price: productData.price,
          date_of_purchase: productData.date_of_purchase,
          period_of_warranty: productData.period_of_warranty,
          retailer_product_id: productData.retailer_product_id,
          product_type: productData.product_type,
          serial_number: productData.serial_number,
          IMEI: productData.IMEI ,
          imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + productData.imgData)
        });
      });
    }

  }

  addProduct() {
    if (this.currentUrl.includes("editProduct"))  {
    console.log(this.productForm.value);
      this.productService.updateProduct(this.productForm.value).subscribe(data => {
        this.router.navigate(["/product"]);
      }) 
    } else {
		    console.log(this.productForm.value);
      this.productService.addProduct(this.productForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/product"]);
      });
    }
    
  }
  getBrands() {
    this.brandService.getBrands().subscribe(data => {
    this.brand = data;
   })
}

getCategories() {
  this.categoryService.getCategory().subscribe(data => {
  this.category = data;
 })
}
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.productForm.patchValue({
      imgData: imgBaseCode
    });
    this.userImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.productForm.get("imgData").setValue("");
    this.userImg = "";
    this.fileInput.nativeElement.value = "";
  }

}
