import { Component, OnInit, ViewChild } from '@angular/core';
import { OffersServiceService }  from '../../services/offers-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";


@Component({
  selector: 'app-offers-management',
  templateUrl: './offers-management.component.html',
  styleUrls: ['./offers-management.component.scss']
})
export class OffersManagementComponent implements OnInit {
  offers: [];
  offersList: [];
  constructor(private offersService: OffersServiceService, private router: Router) { }
  displayedColumns = ['sno','product','productDescription','action'];
  dataSource: MatTableDataSource<offersList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.offersService.getOffers().subscribe(data => {
      this.offers = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  deleteOffers(id,i) {
    this.offers.splice(i,1)
    this.offersService.deleteOffers(id).subscribe(data => {
      console.log(data);
      location.reload();
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.offersList)
    })
  }

}

export interface offersList {
  sno:string;
  product: string;
  productDescription: string;
  action: string;
}
