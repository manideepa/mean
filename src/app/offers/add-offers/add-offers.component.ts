import { Component, OnInit, ViewChild,  ElementRef   } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { OffersServiceService } from "../../services/offers-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-add-offers',
  templateUrl: './add-offers.component.html',
  styleUrls: ['./add-offers.component.scss']
})
export class AddOffersComponent implements OnInit {
@ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  offersForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  base64textString;
  brandImg;
 imageData;

  constructor(
	private fb: FormBuilder,
    private offersService: OffersServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
	  
  this.offersForm = this.fb.group({
      id: [""],
	    //retailerId: [""],
      product: ["", Validators.required],
      productDescription: ["", Validators.required],
      actualPrice: ["", Validators.required],
      discountPrice: ["", Validators.required],
      imgData: [],
      created_at: Date(),
      updated_at:[]
    });

    if (this.currentUrl.includes("editOffers")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.offersService.getOffersById(id).subscribe(data => {
        let offersData = data;
        console.log(offersData);
        this.imageData = offersData.imgData;
        this.offersForm.patchValue({
          id: offersData._id,
          product: offersData.product,
          productDescription: offersData.productDescription,
          actualPrice: offersData.actualPrice,
          discountPrice: offersData.discountPrice,
          imgData: this.brandImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + offersData.imgData),
          updated_at:Date()
        });
      });
    }
  }
  
    addOffers() {
    if (this.currentUrl.includes("editOffers"))  {
    console.log(this.offersForm.value);
      this.offersService.updateOffers(this.offersForm.value).subscribe(data => {
        this.router.navigate(["/offers"]);
      }) 
    } else {
      let retailerId = localStorage.getItem("retailerId")
      let newSale = Object.assign(this.offersForm.value,{'retailerId':retailerId})
      this.offersService.addOffers(this.offersForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/offers"]);
      });
    }
    
  }
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.offersForm.patchValue({
      imgData: imgBaseCode
    });
    this.brandImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.offersForm.get("imgData").setValue("");
    this.brandImg = "";
    this.fileInput.nativeElement.value = "";
  }
}
