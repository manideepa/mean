import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddOffersComponent } from './add-offers/add-offers.component';
import { OffersManagementComponent } from './offers-management/offers-management.component';
import { OffersRoutingModule } from './offers-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OffersServiceService} from './../services/offers-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AddOffersComponent, OffersManagementComponent],
  imports: [
    CommonModule,
    OffersRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    MaterialModule
  ],
  providers: [OffersServiceService]
})
export class OffersModule { }
