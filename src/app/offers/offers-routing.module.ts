import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OffersManagementComponent } from './offers-management/offers-management.component';
import { AddOffersComponent } from './add-offers/add-offers.component';


const routes: Routes = [
    {path: '', component: OffersManagementComponent},
    {path: 'addOffers',component:AddOffersComponent},
    {path: 'editOffers/:id',component: AddOffersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
