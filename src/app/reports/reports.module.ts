import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsManagementComponent } from './reports-management/reports-management.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportsServiceService} from './../services/reports.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [ReportsManagementComponent],
  imports: [
    CommonModule,
      ReportsRoutingModule,
      MaterialModule,
      HttpClientModule,
      ReactiveFormsModule
      
    ],
    exports: [
      MaterialModule
    ],
    providers: [ReportsServiceService]
})
export class ReportsModule { }
