import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SalesServiceService }  from '../../services/sales-service.service';
import { TableUtil } from "./tableUtil";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-reports-management',
  templateUrl: './reports-management.component.html',
  styleUrls: ['./reports-management.component.scss']
})
export class ReportsManagementComponent implements OnInit {

  salesList: [];
  displayedColumns = ['sno','retailer','brand', 'modelNumber', 'price','purchaseDate'];
  dataSource: MatTableDataSource<salesList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('TABLE', { static: true }) table: ElementRef;
  constructor(private salesService: SalesServiceService) { }

  ngOnInit() {
    this.salesService.getSales().subscribe(data => {
      //this.salesList = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    })
  }


  ExportTOExcel()
{
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
  /* save to file */
  XLSX.writeFile(wb, 'SheetJS.xlsx');
  
}

  // exportTable(){
  //   TableUtil.exportToExcel("ExampleTable");
  // }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}

export interface salesList {
  sno:string;
  retailer: string;
  brand: string;
  modelNumber: string;
  price: string;
  purchaseDate: string;
}
