import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    {path: '', component: DashboardComponent,
     children: [{
         path: 'user-management' , loadChildren: '../user-management/user-management.module#UserManagementModule'
     },{
        path: 'customer-management' , loadChildren: '../customer/customer.module#CustomerModule'
    },{
         path: 'retailer', loadChildren: '../retailer-management/retailer-management.module#RetailerManagementModule'
     },{
         path: 'brand', loadChildren: '../brand-management/brand-management.module#BrandManagementModule'
     },{
         path: 'executive-management', loadChildren: '../executive-management/executive-management.module#ExecutiveManagementModule'
     },{
         path: 'product', loadChildren: '../product-management/product-management.module#ProductManagementModule'
     },{
        path: 'category', loadChildren: '../category/category.module#CategoryModule'
    },{
        path: 'sales', loadChildren: '../sales/sales.module#SalesModule'
    },{
        path: 'loyalty-points', loadChildren: '../loyalty-points/loyalty-points.module#LoyaltyPointsModule'
    },{
        path: 'faq', loadChildren: '../faq/faq.module#FaqModule'
    },{
        path: 'service-center', loadChildren: '../service-center/service-center.module#ServiceCenterModule'
    },{
        path: 'offers', loadChildren: '../offers/offers.module#OffersModule'
    },{
        path: 'warranty-requests', loadChildren: '../warranty-requests/warranty-requests.module#WarrantyRequestsModule'
    },{
        path: 'reports', loadChildren: '../reports/reports.module#ReportsModule'
    }]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
