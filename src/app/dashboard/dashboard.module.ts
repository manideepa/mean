import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRoutingModule} from './dashboard-routing.module';
import { MaterialModule } from './../material.module';
import { ClickOutsideModule } from 'ng-click-outside';
import { HighchartsChartModule } from 'highcharts-angular';
@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    ClickOutsideModule,
    HighchartsChartModule
  ],
  exports: [
    MaterialModule
  ]
})
export class DashboardModule { }
