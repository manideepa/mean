import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
//import { AddFaqComponent } from './add-faq/add-faq.component';


const routes: Routes = [
    {path: '', component: CustomerManagementComponent},
    //{path: 'addfaq',component:AddFaqComponent},
    //{path: 'editFaq/:id',component: AddFaqComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
