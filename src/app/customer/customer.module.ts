import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
import { CustomerRoutingModule } from './customer-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerServiceService} from './../services/customer-service.service';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [CustomerManagementComponent],
  imports: [
    CommonModule,
      CustomerRoutingModule,
      MaterialModule,
      HttpClientModule,
      ReactiveFormsModule
      
    ],
    exports: [
      MaterialModule
    ],
    providers: [CustomerServiceService]
})
export class CustomerModule { }
