import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerServiceService }  from '../../services/customer-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {
  customers: [];
  customerList: [];
  displayedColumns = ['sno','firstName','email','mobile'];
  dataSource: MatTableDataSource<customers>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private customerService: CustomerServiceService) { }

  ngOnInit() {
    this.customerService.getCustomers().subscribe(data => {
      this.customerList = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  // deleteSales(id,i) {
  //   this.customerList.splice(i,1)
  //   this.customerService.dele(id).subscribe(data => {
  //     console.log(data);
      
  //     console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.salesList)
  //   })
  // }

}

export interface customers {
  sno:string;
  firstName: string;
  mobile: string;
  email: string;
}
