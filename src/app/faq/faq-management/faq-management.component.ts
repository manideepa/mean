import { Component, OnInit, ViewChild } from '@angular/core';
import { FaqServiceService }  from '../../services/faq-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-faq-management',
  templateUrl: './faq-management.component.html',
  styleUrls: ['./faq-management.component.scss']
})
export class FaqManagementComponent implements OnInit {
  faq: [];
  faqList: [];
  displayedColumns = ['sno','brand','question','action'];
  dataSource: MatTableDataSource<faqList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private faqService: FaqServiceService, private router: Router) { }
 
  ngOnInit() {
	  this.faqService.getFaq().subscribe(data => {
      this.faq = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

   deleteFaq(id,i) {
    this.faq.splice(i,1)
    this.faqService.deleteFaq(id).subscribe(data => {
      console.log(data);
      location.reload();
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.faqList)
    })
  }

}

export interface faqList {
  sno:string;
  brand: string;
  question: string;
  action: string;
}
