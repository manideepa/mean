import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqManagementComponent } from './faq-management/faq-management.component';
import { AddFaqComponent } from './add-faq/add-faq.component';


const routes: Routes = [
    {path: '', component: FaqManagementComponent},
    {path: 'addFaq',component:AddFaqComponent},
    {path: 'editFaq/:id',component: AddFaqComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqRoutingModule { }
