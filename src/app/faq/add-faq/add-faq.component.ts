import { Component, OnInit,ViewChild,  ElementRef   } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { FaqServiceService } from "../../services/faq-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})
export class AddFaqComponent implements OnInit {

  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  faqForm: FormGroup;
  brands = [ ];
  currentUrl = window.location.pathname.split("/");
  base64textString;
  faqImg;
 imageData;

  constructor(
	private fb: FormBuilder,
    private faqService: FaqServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
	  
  this.faqForm = this.fb.group({
      id: [""],
      brand: ["", Validators.required],
      question: ["", Validators.required],
      answer: ["", Validators.required],
      created_at: Date(),
      updated_at:[]
    });

    if (this.currentUrl.includes("editFaq")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.faqService.getFaqById(id).subscribe(data => {
        let faqData = data;
        console.log(faqData);
        this.imageData = faqData.imgData;
        this.faqForm.patchValue({
          id: faqData._id,
          brand: faqData.brand,
          question: faqData.question,
          answer: faqData.answer,
          updated_at: new Date()
        });
      });
    }
  }
  
  getBrands() {
    this.faqService.getBrands().subscribe(data => {
    this.brands = data;
   })
}

    addfaq() {
    if (this.currentUrl.includes("editFaq"))  {
    console.log(this.faqForm.value);
      this.faqService.updateFaq(this.faqForm.value).subscribe(data => {
        this.router.navigate(["/faq"]);
      }) 
    } else {
      this.faqService.addFaq(this.faqForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/faq"]);
      });
    }
    
  }
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.faqForm.patchValue({
      imgData: imgBaseCode
    });
    this.faqImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.faqForm.get("imgData").setValue("");
    this.faqImg = "";
    this.fileInput.nativeElement.value = "";
  }

}
