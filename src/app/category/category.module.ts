import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryManagementComponent } from './category-management/category-management.component';
import { CategoryRoutingModule } from './category-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoryServiceService} from './../services/category-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AddCategoryComponent, CategoryManagementComponent],
  imports: [
      CommonModule,
      CategoryRoutingModule,
      MaterialModule,
      HttpClientModule,
      ReactiveFormsModule
    ],
    exports: [
      MaterialModule
    ],
    providers: [CategoryServiceService]
})
export class CategoryModule { }
