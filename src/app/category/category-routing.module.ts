import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryManagementComponent } from './category-management/category-management.component';
import { AddCategoryComponent } from './add-category/add-category.component';


const routes: Routes = [
    {path: '', component: CategoryManagementComponent},
    {path: 'addCategory',component:AddCategoryComponent},
    {path: 'editCategory/:id',component: AddCategoryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
