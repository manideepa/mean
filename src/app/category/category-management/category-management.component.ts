import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryServiceService }  from '../../services/category-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-category-management',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.scss']
})
export class CategoryManagementComponent implements OnInit {
  categoryData: [];
  categoryList: [];
  displayedColumns = ['sno','category','action'];
  dataSource: MatTableDataSource<categoryList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private categoryService: CategoryServiceService, private router: Router) { }

  ngOnInit() {
    this.categoryService.getCategory().subscribe(data => {
      this.categoryData = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

   deleteCategory(id,i) {
    this.categoryData.splice(i,1)
    this.categoryService.deleteCategory(id).subscribe(data => {
      //console.log(data);
      location.reload();
      //this.router.navigate(["/brand"]);
    })
  }

}
export interface categoryList {
  sno:string;
  category: string;
  action: string;
}
