import { Component, OnInit, ViewChild,  ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { CategoryServiceService } from "../../services/category-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  categoryForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  base64textString;
  brandImg;
  imageData;

  constructor(
    private fb: FormBuilder,
    private categoryService: CategoryServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    this.categoryForm = this.fb.group({
      id: [""],
      category: ["", Validators.required],
      imgData: [],
      created_at: Date(),
      updated_at:[]
    });

    if (this.currentUrl.includes("editCategory")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.categoryService.getCategoryById(id).subscribe(data => {
        let categoryData = data;
        this.imageData = categoryData.imgData;
        this.categoryForm.patchValue({
          id: categoryData._id,
          category: categoryData.category,
          imgData: this.brandImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + categoryData.imgData),
          updated_at: new Date()
        });
      });
    }
  }

    
  addCategory() {
    if (this.currentUrl.includes("editCategory"))  {
      this.categoryService.updateCategory(this.categoryForm.value).subscribe(data => {
        this.router.navigate(["/category"]);
      }) 
    } else {
      this.categoryService.addCategory(this.categoryForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/category"]);
      });
    }
    
  }
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.categoryForm.patchValue({
      imgData: imgBaseCode
    });
    this.brandImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.categoryForm.get("imgData").setValue("");
    this.brandImg = "";
    this.fileInput.nativeElement.value = "";
  }

}
