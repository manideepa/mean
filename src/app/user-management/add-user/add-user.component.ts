import { Component, OnInit,ViewChild,  ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { UserServiceService } from "../../services/user-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
        @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  userForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  base64textString;
  userImg;
 imageData
  constructor(
    private fb: FormBuilder,
    private userService: UserServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {
    this.userForm = this.fb.group({
      id: [""],
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      login: ["", Validators.required],
      email: ["", Validators.required],
      roles: ["", Validators.required],
	  password: ["", Validators.required],
	  lattitude: ["", Validators.required],
	  longitude: ["", Validators.required],
	  additional_one: ["", Validators.required],
	  additional_two: ["", Validators.required],
	  additional_three: ["", Validators.required],
	  additional_four: ["", Validators.required],
      imgData: [],
      pointPerAmount:["", Validators.required]
    });

    if (this.currentUrl.includes("editUser")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      
      this.userService.getUserById(id).subscribe(data => {
        let userData = data;
        console.log(userData);
        this.imageData = userData.imgData;
        this.userForm.patchValue({
          id: userData.id,
          firstName: userData.firstName,
          lastName: userData.lastName,
          login: userData.login,
          email: userData.email,
          roles: userData.roles,
		      password: userData.password,
		      lattitude: userData.lattitude,
		      longitude: userData.longitude,
		      additional_one: userData.additional_one,
		      additional_two: userData.additional_two,
		      additional_three: userData.additional_three,
          additional_four: userData.additional_four,
          pointPerAmount: userData.pointPerAmount,
          imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + userData.imgData)
        });
      });
    }
  }
  addUser() {
    if (this.currentUrl.includes("editUser"))  {
      console.log(this.userForm.value);
      this.userService.updateUser(this.userForm.value).subscribe(data => {
        this.router.navigate(["/user-management"]);
      }) 
    } else {
      this.userService.addUser(this.userForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/user-management"]);
      });
    }
    
  }
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.userForm.patchValue({
      imgData: imgBaseCode
    });
    this.userImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.userForm.get("imgData").setValue("");
    this.userImg = "";
    this.fileInput.nativeElement.value = "";
  }
}
