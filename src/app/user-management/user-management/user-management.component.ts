import { Component, OnInit, ViewChild } from '@angular/core';
import { UserServiceService }  from '../../services/user-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  usersList: [];

  displayedColumns = ['sno','firstName', 'lastName', 'email', 'roles','action'];
  dataSource: MatTableDataSource<usersList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(private userService: UserServiceService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(data => {
      //this.usersList = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    

    })
  }

  

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  
  deleteUser(id,i) {
    this.usersList.splice(i,1)
    this.userService.deleteUser(id).subscribe(data => {
      console.log(data);
      
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.usersList)
    })
  }

  

}

export interface usersList {
  sno:string;
  firstName: string;
  lastName: string;
  email: string;
  roles: string;
  action: string;
}
