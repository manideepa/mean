import { TestBed } from '@angular/core/testing';

import { RetailerServiceService } from './retailer-service.service';

describe('RetailerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RetailerServiceService = TestBed.get(RetailerServiceService);
    expect(service).toBeTruthy();
  });
});
