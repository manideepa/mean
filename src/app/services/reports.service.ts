import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class ReportsServiceService {

  constructor(private http: HttpClient) { }

  getReport() : Observable<any> {
    return this.http.get(`${environment.baseUrl}reports/getReport`);
  }
  getSales() : Observable<any> {
    return this.http.get(`${environment.baseUrl}reports/getSales`);
  }
  getReportById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}reports/  getReportById(id) : Observable<any> {
      ?id=${id}`)
  }
}
