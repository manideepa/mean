import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class LoyaltyPointsServiceService {

  constructor(private http: HttpClient) { }

  addLoyaltyPoints(loyaltyPointsObject):Observable<any> {
    console.log(loyaltyPointsObject);
    return this.http.post(`${environment.baseUrl}loyalty-points/addLoyaltyPoints`,loyaltyPointsObject)
  } 
  updateLoyaltyPoints(loyaltyPointsObject):Observable<any> {
    console.log(loyaltyPointsObject);
    return this.http.put(`${environment.baseUrl}loyalty-points/updateLoyaltyPoints`,loyaltyPointsObject)
  } 
  checkLoyaltyPoints(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}loyalty-points/checkLoyaltyPoints`,loginObject);
  }
  deleteLoyaltyPoints(loyaltyPointsId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}loyalty-points/deleteLoyaltyPoints?id=`+loyaltyPointsId)
  }
  getLoyaltyPoints() : Observable<any> {
    return this.http.get(`${environment.baseUrl}loyalty-points/getLoyaltyPoints`);
  }
  getLoyaltyPointsById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}loyalty-points/getLoyaltyPointsById?id=`+id)
  }
}
