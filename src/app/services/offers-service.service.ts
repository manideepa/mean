import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class OffersServiceService {

  constructor(private http: HttpClient) { }

  addOffers(offersObject):Observable<any> {
    console.log(offersObject);
    return this.http.post(`${environment.baseUrl}offers/addOffers`,offersObject)
  } 
  updateOffers(offersObject):Observable<any> {
    console.log(offersObject);
    return this.http.put(`${environment.baseUrl}offers/updateOffers`,offersObject)
  } 
  checkOffers(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}offers/checkfaq`,loginObject);
  } 
  getOffersInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}offers/getOffers?token=`+token)
  }
  deleteOffers(offersId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}offers/deleteOffers?id=`+offersId)
  }
  getOffers() : Observable<any> {
    return this.http.get(`${environment.baseUrl}offers/getOffers`);
  }
  getOffersById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}Offers/getOffersById?id=`+id)
  }
}
