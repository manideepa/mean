import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class ServiceCenterServiceService {

  constructor(private http: HttpClient) { }

  addServiceCenter(serviceCenterObject):Observable<any> {
    console.log(serviceCenterObject);
    return this.http.post(`${environment.baseUrl}serviceCenter/addServiceCenter`,serviceCenterObject)
  } 
  getServiceCenters() : Observable<any> {
    return this.http.get(`${environment.baseUrl}serviceCenter/getServiceCenters`);
  }
  getServiceCenterById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}serviceCenter/getServiceCenterById?id=`+id)
  }
  deleteServiceCenter(serviceCenterId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}serviceCenter/deleteServiceCenter?id=`+serviceCenterId)
  }
  updateServiceCenter(executiveObject):Observable<any> {
    return this.http.put(`${environment.baseUrl}serviceCenter/updateServiceCenter`,executiveObject)
  }
}
