import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class CustomerServiceService {

  constructor(private http: HttpClient) { }

  getCustomer() : Observable<any> {
    return this.http.get(`${environment.baseUrl}customers/getCustomer`);
  }
  getCustomers() : Observable<any> {
    return this.http.get(`${environment.baseUrl}customers/getCustomers`);
  }
  getCustomerById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}customers/getCustomerById?id=${id}`)
  }
}
