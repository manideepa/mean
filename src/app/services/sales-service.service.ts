import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class SalesServiceService {

  constructor(private http: HttpClient) { }

  addSales(salesObject):Observable<any> {
    console.log(salesObject);
    return this.http.post(`${environment.baseUrl}sales/addSales`,salesObject)
  } 
  updateSales(salesObject):Observable<any> {
    console.log(salesObject);
    return this.http.put(`${environment.baseUrl}sales/updateSales`,salesObject)
  } 
  checkSales(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}sales/checksales`,loginObject);
  } 
  getSalesInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}sales/getSales?token=`+token)
  }
  deleteSales(salesId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}sales/deleteSales?id=`+salesId)
  }
  getSales() : Observable<any> {
    return this.http.get(`${environment.baseUrl}sales/getSales`);
  }
  getSalesById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}sales/getSalesById?id=`+id)
  }
}
