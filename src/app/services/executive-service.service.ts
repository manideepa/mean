import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class ExecutiveServiceService {

  constructor(private http: HttpClient) { }

  addExecutive(executiveObject):Observable<any> {
    return this.http.post(`${environment.baseUrl}executives/addExecutive`,executiveObject)
  } 
  updateExecutive(executiveObject):Observable<any> {
    return this.http.put(`${environment.baseUrl}executives/updateExecutive`,executiveObject)
  } 
  checkExecutive(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}executives/checkexecutive`,loginObject);
  } 
  getExecutiveInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}executives/getExecutive?token=`+token)
  }
  deleteExecutive(executiveId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}executives/deleteExecutive?id=`+executiveId)
  }
  getExecutives() : Observable<any> {
    return this.http.get(`${environment.baseUrl}executives/getExecutives`);
  }
  getExecutiveById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}executives/gerExecutiveById?id=`+id)
  }
}
