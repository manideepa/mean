import { TestBed } from '@angular/core/testing';

import { WarrantyRequestsServiceService } from './warranty-requests-service.service';

describe('CustomerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarrantyRequestsServiceService = TestBed.get(WarrantyRequestsServiceService);
    expect(service).toBeTruthy();
  });
});
