import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private http: HttpClient) { }

  addProduct(productObject):Observable<any> {
    console.log(productObject);
    return this.http.post(`${environment.baseUrl}products/addProduct`,productObject)
  } 
  updateProduct(productObject):Observable<any> {
    console.log(productObject);
    return this.http.put(`${environment.baseUrl}products/updateProduct`,productObject)
  } 
  checkProduct(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}products/checkproduct`,loginObject);
  } 
  getProductInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}products/getProduct?token=`+token)
  }
  deleteProduct(productId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}products/deleteProduct?id=`+productId)
  }
  getProducts() : Observable<any> {
    return this.http.get(`${environment.baseUrl}products/getProducts`);
  }
  getProductById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}products/getProductById?id=`+id)
  }
}
