import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class CategoryServiceService {

  constructor(private http: HttpClient) { }

  addCategory(categoryObject):Observable<any> {
    console.log(categoryObject);
    return this.http.post(`${environment.baseUrl}category/addCategory`,categoryObject)
  } 
  updateCategory(categoryObject):Observable<any> {
    console.log(categoryObject);
    return this.http.put(`${environment.baseUrl}category/updateCategory`,categoryObject)
  } 
  checkCategory(categoryObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}category/checkCategory`,categoryObject);
  } 
  getCategoryInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}category/getCategory?token=`+token)
  }
  deleteCategory(categoryId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}category/deleteCategory?id=`+categoryId)
  }
  getCategory() : Observable<any> {
    return this.http.get(`${environment.baseUrl}category/getCategory`);
  }
  getCategoryById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}category/getCategoryById?id=`+id)
  }
}
