import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class FaqServiceService {

  constructor(private http: HttpClient) { }

  addFaq(faqObject):Observable<any> {
    console.log(faqObject);
    return this.http.post(`${environment.baseUrl}faq/addFaq`,faqObject)
  } 
  updateFaq(faqObject):Observable<any> {
    console.log(faqObject);
    return this.http.put(`${environment.baseUrl}faq/updateFaq`,faqObject)
  } 
  checkFaq(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}faq/checkfaq`,loginObject);
  } 
  getFaqInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}faq/getFaq?token=`+token)
  }
  deleteFaq(faqId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}faq/deleteFaq?id=`+faqId)
  }
  getFaq() : Observable<any> {
    return this.http.get(`${environment.baseUrl}faq/getFaqs`);
  }
  getFaqById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}faq/getFaqById?id=`+id)
  }
  getBrands() : Observable<any> {
    return this.http.get(`${environment.baseUrl}faq/getBrands`);
  }
}
