import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class WarrantyRequestsServiceService {

  constructor(private http: HttpClient) { }

  addWarrantyRequests(warrantyRequestsObject):Observable<any> {
    console.log(warrantyRequestsObject);
    return this.http.post(`${environment.baseUrl}warrantyRequests/addWarrantyRequests`,warrantyRequestsObject)
  } 
  updateWarrantyRequests(warrantyRequestsObject):Observable<any> {
    console.log(warrantyRequestsObject);
    return this.http.put(`${environment.baseUrl}warrantyRequests/updateWarrantyRequests`,warrantyRequestsObject)
  } 
  checkWarrantyRequests(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}warrantyRequests/checkWarrantyRequests`,loginObject);
  } 
  getWarrantyRequestsInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}warrantyRequests/getWarrantyRequestsInfo?token=`+token)
  }
  deleteWarrantyRequests(warrantyRequestsId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}warrantyRequests/deleteWarrantyRequests?id=`+warrantyRequestsId)
  }
  getWarrantyRequests() : Observable<any> {
    return this.http.get(`${environment.baseUrl}warrantyRequests/getWarrantyRequests`);
  }
  getWarrantyRequestsById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}warrantyRequests/getWarrantyRequestsById?id=`+id)
  }
}
