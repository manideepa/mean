import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class RetailerServiceService {

  constructor(private http: HttpClient) { }

  addRetailer(retailerObject):Observable<any> {
    return this.http.post(`${environment.baseUrl}retailers/addRetailer`,retailerObject)
  } 
  getRetailer():Observable<any> {
    return this.http.get(`${environment.baseUrl}retailers/getRetailers`)
  } 
  updateRetailer(retailerObject):Observable<any> {
    return this.http.put(`${environment.baseUrl}retailers/updateRetailer`,retailerObject)
  } 
  checkRetailer(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}retailers/checkretailer`,loginObject);
  } 
  getRetailerInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}retailers/getRetailer?token=`+token)
  }
  deleteRetailer(retailerId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}retailers/deleteRetailer?id=`+retailerId)
  }
  getRetailers() : Observable<any> {
    return this.http.get(`${environment.baseUrl}retailers/getRetailers`);
  }
  getRetailerById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}retailers/gerRetailerById?id=`+id)
  }
}
