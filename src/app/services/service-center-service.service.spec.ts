import { TestBed } from '@angular/core/testing';

import { ServiceCenterServiceService } from './service-center-service.service';

describe('SalesServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceCenterServiceService = TestBed.get(ServiceCenterServiceService);
    expect(service).toBeTruthy();
  });
});
