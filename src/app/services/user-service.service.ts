import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }

  addUser(userObject):Observable<any> {
    return this.http.post(`${environment.baseUrl}users/addUser`,userObject)
  } 
  updateUser(userObject):Observable<any> {
    return this.http.put(`${environment.baseUrl}users/updateUser`,userObject)
  } 
  checkUser(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}users/checkuser`,loginObject);
  } 
  getUserInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}users/getUser?token=`+token)
  }
  deleteUser(userId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}users/deleteUser?id=`+userId)
  }
  getUsers() : Observable<any> {
    return this.http.get(`${environment.baseUrl}users/getUsers`);
  }
  getRetailers() : Observable<any> {
    return this.http.get(`${environment.baseUrl}users/getRetailers`);
  }
  getUserById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}users/gerUserById?id=`+id)
  }
  getRetailersCount() : Observable<any> {
    console.log(22);
    return this.http.get(`${environment.baseUrl}users/getRetailersCount`)
  }
  getSalesCount() : Observable<any> {
    console.log(22);
    return this.http.get(`${environment.baseUrl}users/getSalesCount`)
  }
  getCustomersCount() : Observable<any> {
    console.log(22);
    return this.http.get(`${environment.baseUrl}users/getCustomersCount`)
  }
}
