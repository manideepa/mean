import { TestBed } from '@angular/core/testing';

import { LoyaltyPointsServiceService } from './loyalty-points-service.service';

describe('LoyaltyPointsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoyaltyPointsServiceService = TestBed.get(LoyaltyPointsServiceService);
    expect(service).toBeTruthy();
  });
});
