import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class BrandServiceService {

  constructor(private http: HttpClient) { }

  addBrand(brandObject):Observable<any> {
    return this.http.post(`${environment.baseUrl}brands/addBrand`,brandObject)
  } 
  updateBrand(brandObject):Observable<any> {
    return this.http.put(`${environment.baseUrl}brands/updateBrand`,brandObject)
  } 
  checkBrand(loginObject) : Observable<any> {
    return this.http.post(`${environment.baseUrl}brands/checkbrand`,loginObject);
  } 
  getBrandInfo(token) : Observable<any> {
    return this.http.get(`${environment.baseUrl}brands/getBrand?token=`+token)
  }
  deleteBrand(brandId) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}brands/deleteBrand?id=`+brandId)
  }
  getBrands() : Observable<any> {
    return this.http.get(`${environment.baseUrl}brands/getBrands`);
  }
  getBrandById(id) : Observable<any> {
    return this.http.get(`${environment.baseUrl}brands/getBrandById?id=`+id)
  }
}
