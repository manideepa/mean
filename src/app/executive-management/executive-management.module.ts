import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExecutiveManagementComponent } from './executive-management/executive-management.component';
import { AddExecutiveComponent } from './add-executive/add-executive.component';
import { ExecutiveRoutingModule } from './executive-management-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExecutiveServiceService} from './../services/executive-service.service';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [ExecutiveManagementComponent, AddExecutiveComponent],
  imports: [
    CommonModule,
    ExecutiveRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    MaterialModule
  ],
  providers: [ExecutiveServiceService]
})
export class ExecutiveManagementModule { }
