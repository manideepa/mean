import { Component, OnInit } from '@angular/core';
import { ExecutiveServiceService }  from '../../services/executive-service.service';


@Component({
  selector: 'app-executive-management',
  templateUrl: './executive-management.component.html',
  styleUrls: ['./executive-management.component.scss']
})
export class ExecutiveManagementComponent implements OnInit {
  executivesList: [];
  constructor(private executiveService: ExecutiveServiceService) { }

  ngOnInit() {
	  this.executiveService.getExecutives().subscribe(data => {
      this.executivesList = data;
      console.log("executige list",this.executivesList)
    })
  }
  deleteExecutive(id,i) {
    this.executivesList.splice(i,1)
    this.executiveService.deleteExecutive(id).subscribe(data => {
      console.log(data);
      
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.executivesList)
    })
  }
}
