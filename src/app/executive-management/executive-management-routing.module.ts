import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExecutiveManagementComponent } from './executive-management/executive-management.component';
import { AddExecutiveComponent } from './add-executive/add-executive.component';


const routes: Routes = [
    {path: '', component: ExecutiveManagementComponent},
    {path: 'addexecutive',component:AddExecutiveComponent},
    {path: 'editExecutive/:id',component: AddExecutiveComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutiveRoutingModule { }
