import { Component, OnInit,ViewChild,  ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { ExecutiveServiceService } from "../../services/executive-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { UserServiceService }  from '../../services/user-service.service';


@Component({
  selector: 'app-add-executive',
  templateUrl: './add-executive.component.html',
  styleUrls: ['./add-executive.component.scss']
})
export class AddExecutiveComponent implements OnInit {
  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  executiveForm: FormGroup;
  retailers = [ ];
  retailerList = [];
  currentUrl = window.location.pathname.split("/");
  base64textString;
  userImg;
  imageData;
  constructor(
    private fb: FormBuilder,
    private executiveService: ExecutiveServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private userService: UserServiceService
  ) { 
    this.executiveForm = this.fb.group({
      retailers: ['']
    });
  }

  

  ngOnInit() {
	  this.executiveForm = this.fb.group({
      id: [""],
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      email: ["", Validators.required],
      retailers: ["", Validators.required],
	    address: ["", Validators.required],
	    password: ["", Validators.required],
      imgData: []
    });

    this.getRetailers();
    

    if (this.currentUrl.includes("editExecutive")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      
      this.executiveService.getExecutiveById(id).subscribe(data => {
        let executiveData = data;
        console.log(executiveData);
        this.imageData = executiveData.imgData;
        this.executiveForm.patchValue({
          id: executiveData.id,
          firstName: executiveData.firstName,
          lastName: executiveData.lastName,
          address: executiveData.address,
          email: executiveData.email,
          retailers: executiveData.retailers,
		  password: executiveData.password,
		
          imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + executiveData.imgData)
        });
      });
    }
  }

  getRetailers() {
      this.userService.getRetailers().subscribe(data => {
      this.retailers = data;
     })
  }
   addExecutive() {
    if (this.currentUrl.includes("editExecutive"))  {
      this.executiveService.updateExecutive(this.executiveForm.value).subscribe(data => {
        this.router.navigate(["/executive-management"]);
      }) 
    } else {
      this.executiveService.addExecutive(this.executiveForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/executive-management"]);
      });
    }
    
  }
 onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.executiveForm.patchValue({
      imgData: imgBaseCode
    });
    this.userImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.executiveForm.get("imgData").setValue("");
    this.userImg = "";
    this.fileInput.nativeElement.value = "";
  }
}
