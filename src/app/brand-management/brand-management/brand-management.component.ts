import { Component, OnInit, ViewChild } from '@angular/core';
import { BrandServiceService }  from '../../services/brand-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";



@Component({
  selector: 'app-brand-management',
  templateUrl: './brand-management.component.html',
  styleUrls: ['./brand-management.component.scss']
})
export class BrandManagementComponent implements OnInit {
  brandsData: [];
  brandsList: [];
  displayedColumns = ['sno','brand','action'];
  dataSource: MatTableDataSource<brandsList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private brandService: BrandServiceService, private router: Router) { }

  ngOnInit() {
	  this.brandService.getBrands().subscribe(data => {
      this.brandsData = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

   deleteBrand(id,i) {
    this.brandsData.splice(i,1)
    this.brandService.deleteBrand(id).subscribe(data => {
      //console.log(data);
      location.reload();
      //this.router.navigate(["/brand"]);
    })
  }

}

export interface brandsList {
  sno:string;
  brand: string;
  action: string;
}
