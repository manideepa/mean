import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBrandComponent } from './add-brand/add-brand.component';
import { BrandManagementComponent } from './brand-management/brand-management.component';
import { BrandRoutingModule } from './brand-management-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrandServiceService} from './../services/brand-service.service';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [AddBrandComponent, BrandManagementComponent],
  imports: [
    CommonModule,
    BrandRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    MaterialModule
  ],
  providers: [BrandServiceService]
})
export class BrandManagementModule { }
