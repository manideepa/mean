import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandManagementComponent } from './brand-management/brand-management.component';
import { AddBrandComponent } from './add-brand/add-brand.component';


const routes: Routes = [
    {path: '', component: BrandManagementComponent},
    {path: 'addBrand',component:AddBrandComponent},
    {path: 'editBrand/:id',component: AddBrandComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }
