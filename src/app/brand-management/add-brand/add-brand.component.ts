import { Component, OnInit,ViewChild,  ElementRef   } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { BrandServiceService } from "../../services/brand-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-add-brand',
  templateUrl: './add-brand.component.html',
  styleUrls: ['./add-brand.component.scss']
})
export class AddBrandComponent implements OnInit {
	@ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  brandForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  base64textString;
  brandImg;
 imageData;

  constructor(
	private fb: FormBuilder,
    private brandService: BrandServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
	  
  this.brandForm = this.fb.group({
      id: [""],
      brand: ["", Validators.required],
      email: ["", Validators.required],
      mobile: ["", Validators.required],
      warrantyTime: ["", Validators.required],
      customerAssistance: ["", Validators.required],
      warrantySupport: ["", Validators.required],
      locations: ["", Validators.required],
      rating: ["", Validators.required],
      imgData: [],
      created_at: Date(),
      updated_at:[]
    });

    if (this.currentUrl.includes("editBrand")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.brandService.getBrandById(id).subscribe(data => {
        let brandData = data;
        console.log(brandData);
        this.imageData = brandData.imgData;
        this.brandForm.patchValue({
          id: brandData._id,
          brand: brandData.brand,
          email: brandData.email,
          mobile: brandData.mobile,
          warrantyTime: brandData.warrantyTime,
          customerAssistance: brandData.customerAssistance,
          warrantySupport: brandData.warrantySupport,
          locations: brandData.locations,
          rating: brandData.rating,
          imgData: this.brandImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + brandData.imgData),
          updated_at: new Date()
        });
      });
    }
  }
  
    addBrand() {
    if (this.currentUrl.includes("editBrand"))  {
    console.log(this.brandForm.value);
      this.brandService.updateBrand(this.brandForm.value).subscribe(data => {
        this.router.navigate(["/brand"]);
      }) 
    } else {
      this.brandService.addBrand(this.brandForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/brand"]);
      });
    }
    
  }
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.brandForm.patchValue({
      imgData: imgBaseCode
    });
    this.brandImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.brandForm.get("imgData").setValue("");
    this.brandImg = "";
    this.fileInput.nativeElement.value = "";
  }

}
