import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddWarrantyRequestsComponent } from './add-warranty-requests/add-warranty-requests.component';
import { WarrantyRequestsManagementComponent } from './warranty-requests-management/warranty-requests-management.component';
import { WarrantyRequestsRoutingModule } from './warranty-requests-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WarrantyRequestsServiceService} from './../services/warranty-requests-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AddWarrantyRequestsComponent, WarrantyRequestsManagementComponent],
  imports: [
      CommonModule,
      WarrantyRequestsRoutingModule,
      MaterialModule,
      HttpClientModule,
      ReactiveFormsModule
      
    ],
    exports: [
      MaterialModule
    ],
    providers: [WarrantyRequestsServiceService]
})
export class WarrantyRequestsModule { }
