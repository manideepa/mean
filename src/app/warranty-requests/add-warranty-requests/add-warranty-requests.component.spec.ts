import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWarrantyRequestsComponent } from './add-warranty-requests.component';

describe('AddWarrantyRequestsComponent', () => {
  let component: AddWarrantyRequestsComponent;
  let fixture: ComponentFixture<AddWarrantyRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddWarrantyRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWarrantyRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
