import { Component, OnInit,ViewChild,  ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { WarrantyRequestsServiceService } from "../../services/warranty-requests-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-add-warranty-requests',
  templateUrl: './add-warranty-requests.component.html',
  styleUrls: ['./add-warranty-requests.component.scss']
})
export class AddWarrantyRequestsComponent implements OnInit {
  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  warrantyRequestForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  constructor(
    private fb: FormBuilder,
    private warrantyRequestService: WarrantyRequestsServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {
    this.warrantyRequestForm = this.fb.group({
      id: [""],
      warrantyRequestId: ["", Validators.required],
      salesId: ["", Validators.required],
      customerId: ["", Validators.required],
      contactTime: ["", Validators.required],
      
    });

    if (this.currentUrl.includes("editWarrantyRequests")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      
      this.warrantyRequestService.getWarrantyRequestsById(id).subscribe(data => {
        let warrantyRequestData = data;
        console.log(warrantyRequestData);
        this.warrantyRequestForm.patchValue({
          id: warrantyRequestData.id,
          warrantyRequestId: warrantyRequestData.warrantyRequestId,
          salesId: warrantyRequestData.salesId,
          customerId: warrantyRequestData.customerId,
          contactTime: warrantyRequestData.contactTime,
          });
      });
    }
  }
  addWarrantyRequest() {
    if (this.currentUrl.includes("editWarrantyRequests"))  {
      console.log(this.warrantyRequestForm.value);
      this.warrantyRequestService.updateWarrantyRequests(this.warrantyRequestForm.value).subscribe(data => {
        this.router.navigate(["/warranty-requests"]);
      }) 
    } else {
      this.warrantyRequestService.addWarrantyRequests(this.warrantyRequestForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/warranty-requests"]);
      });
    }
    
  }


}
