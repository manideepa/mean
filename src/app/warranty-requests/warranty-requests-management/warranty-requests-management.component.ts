import { Component, OnInit } from '@angular/core';
import { WarrantyRequestsServiceService }  from '../../services/warranty-requests-service.service';

@Component({
  selector: 'app-warranty-requests-management',
  templateUrl: './warranty-requests-management.component.html',
  styleUrls: ['./warranty-requests-management.component.scss']
})
export class WarrantyRequestsManagementComponent implements OnInit {

  warrantyRequestsList: [];
  constructor(private warrantyRequestService: WarrantyRequestsServiceService) { }

  ngOnInit() {
    this.warrantyRequestService.getWarrantyRequests().subscribe(data => {
      console.log(data)
      this.warrantyRequestsList = data;
    })
  }
  deleteUser(id,i) {
    this.warrantyRequestsList.splice(i,1)
    this.warrantyRequestService.deleteWarrantyRequests(id).subscribe(data => {
      console.log(data);
      
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.warrantyRequestsList)
    })
  }

}
