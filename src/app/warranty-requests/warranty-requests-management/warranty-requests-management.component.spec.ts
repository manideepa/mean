import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantyRequestsManagementComponent } from './warranty-requests-management.component';

describe('WarrantyRequestsManagementComponent', () => {
  let component: WarrantyRequestsManagementComponent;
  let fixture: ComponentFixture<WarrantyRequestsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantyRequestsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyRequestsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
