import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarrantyRequestsManagementComponent } from './warranty-requests-management/warranty-requests-management.component';
import { AddWarrantyRequestsComponent } from './add-warranty-requests/add-warranty-requests.component';


const routes: Routes = [
    {path: '', component: WarrantyRequestsManagementComponent},
    {path: 'addwarrantyrequests',component:AddWarrantyRequestsComponent},
    {path: 'editWarrantyRequests/:id',component: AddWarrantyRequestsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarrantyRequestsRoutingModule { }
