import { Component, OnInit, ViewChild } from '@angular/core';
import { RetailerServiceService } from '../../services/retailer-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-retailer-management',
  templateUrl: './retailer-management.component.html',
  styleUrls: ['./retailer-management.component.scss']
})
export class RetailerManagementComponent implements OnInit {
  retailersList;
  retailers: [];
  displayedColumns = ['sno','firstName','email'];
  dataSource: MatTableDataSource<retailers>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private retailerService:RetailerServiceService, private router: Router) { }

  ngOnInit() {
    this.retailerService.getRetailers().subscribe(data => {
      this.retailersList = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
export interface retailers {
  sno:string;
  firstName: string;
  email: string;
}
