import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RetailerManagementComponent } from './retailer-management/retailer-management.component';
import { AddRetailerComponent } from './add-retailer/add-retailer.component';


const routes: Routes = [
    {path: '', component: RetailerManagementComponent},
    {path: 'addretailer',component:AddRetailerComponent},
    {path: 'editRetailer/:id',component: AddRetailerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RetailerRoutingModule { }
