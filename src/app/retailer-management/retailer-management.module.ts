import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddRetailerComponent } from './add-retailer/add-retailer.component';
import { RetailerManagementComponent } from './retailer-management/retailer-management.component';
import { RetailerRoutingModule } from './retailer-management-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RetailerServiceService} from './../services/retailer-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AddRetailerComponent, RetailerManagementComponent],
  imports: [
    CommonModule,
    RetailerRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    MaterialModule
  ],
  providers: [RetailerServiceService]
})
export class RetailerManagementModule { }
