import { Component, OnInit, ViewChild,  ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { SalesServiceService } from "../../services/sales-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-add-sales',
  templateUrl: './add-sales.component.html',
  styleUrls: ['./add-sales.component.scss']
})
export class AddSalesComponent implements OnInit {
  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  salesForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  base64textString;
  userImg;
 imageData
  constructor(
  private fb: FormBuilder,
    private salesService: SalesServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
	  
  this.salesForm = this.fb.group({
      id: [""],
      model: ["", Validators.required],
      brand: ["", Validators.required],
      model_number: ["", Validators.required],
      price: ["", Validators.required],
	  date_of_purchase: ["", Validators.required],
	  period_of_warranty: ["", Validators.required],
	  retailer_product_id: ["", Validators.required],
	  product_type: ["", Validators.required],
	  serial_number: ["", Validators.required],
	  IMEI: ["", Validators.required],
      imgData: []
    });

    if (this.currentUrl.includes("editSales")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.salesService.getSalesById(id).subscribe(data => {
        let salesData = data;
        console.log(salesData);
        this.imageData = salesData.imgData;
        this.salesForm.patchValue({
          id: salesData._id,
          model: salesData.model,
          brand: salesData.brand,
          model_number: salesData.model_number,
          price: salesData.price,
		  date_of_purchase: salesData.date_of_purchase,
		  period_of_warranty: salesData.period_of_warranty,
		  retailer_product_id: salesData.retailer_product_id,
		  product_type: salesData.product_type,
		  serial_number: salesData.serial_number,
 		  IMEI: salesData.IMEI ,
          imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + salesData.imgData)
        });
      });
    }
  }
  
    addSales() {
    if (this.currentUrl.includes("editSales"))  {
    console.log(this.salesForm.value);
      this.salesService.updateSales(this.salesForm.value).subscribe(data => {
        this.router.navigate(["/sales"]);
      }) 
    } else {
        console.log(this.salesForm.value);
      let retailerId = localStorage.getItem("retailerId")
      let newSale = Object.assign(this.salesForm.value,{'retailerId':retailerId})
      this.salesService.addSales(this.salesForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/sales"]);
      });
    }
    
  }
  onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.salesForm.patchValue({
      imgData: imgBaseCode
    });
    this.userImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.salesForm.get("imgData").setValue("");
    this.userImg = "";
    this.fileInput.nativeElement.value = "";
  }

}
