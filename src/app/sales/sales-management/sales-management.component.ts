import { Component, OnInit } from '@angular/core';
import { SalesServiceService }  from '../../services/sales-service.service';

@Component({
  selector: 'app-sales-management',
  templateUrl: './sales-management.component.html',
  styleUrls: ['./sales-management.component.scss']
})
export class SalesManagementComponent implements OnInit {

  salesList: [];
  constructor(private salesService: SalesServiceService) { }

  ngOnInit() {
    this.salesService.getSales().subscribe(data => {
      this.salesList = data;
    })
  }
  deleteSales(id,i) {
    this.salesList.splice(i,1)
    this.salesService.deleteSales(id).subscribe(data => {
      console.log(data);
      
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.salesList)
    })
  }

}
