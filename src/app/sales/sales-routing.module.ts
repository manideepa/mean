import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesManagementComponent } from './sales-management/sales-management.component';
import { AddSalesComponent } from './add-sales/add-sales.component';


const routes: Routes = [
    {path: '', component: SalesManagementComponent},
    {path: 'addsales',component:AddSalesComponent},
    {path: 'editSales/:id',component: AddSalesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
