import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSalesComponent } from './add-sales/add-sales.component';
import { SalesManagementComponent } from './sales-management/sales-management.component';
import { SalesRoutingModule } from './sales-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SalesServiceService} from './../services/sales-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AddSalesComponent, SalesManagementComponent],
  imports: [
	CommonModule,
    SalesRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule
    
  ],
  exports: [
    MaterialModule
  ],
  providers: [SalesServiceService]
})
export class SalesModule { }
