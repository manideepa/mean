import { Component, OnInit, ViewChild } from '@angular/core';
import { LoyaltyPointsServiceService }  from '../../services/loyalty-points-service.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-loyalty-points-management',
  templateUrl: './loyalty-points-management.component.html',
  styleUrls: ['./loyalty-points-management.component.scss']
})
export class LoyaltyPointsManagementComponent implements OnInit {
  loyaltypoints: [];
  loyaltypointsList: [];
  displayedColumns = ['sno','price','points','created_at','modified_at','action'];
  dataSource: MatTableDataSource<loyaltypointsList>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private loyaltyPointsServiceService: LoyaltyPointsServiceService, private router: Router) { }

  ngOnInit() {
    this.loyaltyPointsServiceService.getLoyaltyPoints().subscribe(data => {
      this.loyaltypoints = data;
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  deleteLoyaltyPoints(id,i) {
    this.loyaltypoints.splice(i,1)
    this.loyaltyPointsServiceService.deleteLoyaltyPoints(id).subscribe(data => {
      console.log(data);
      
      console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.loyaltypointsList)
    })
  }

}
export interface loyaltypointsList {
  sno:string;
  price: string;
  points: string;
  created_at: string;
  modified_at: string;
  action: string;
}
