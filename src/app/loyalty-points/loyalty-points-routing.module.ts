import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoyaltyPointsManagementComponent } from './loyalty-points-management/loyalty-points-management.component';
import { AddLoyaltyPointsComponent } from './add-loyalty-points/add-loyalty-points.component';


const routes: Routes = [
    {path: '', component: LoyaltyPointsManagementComponent},
    {path: 'addloyalitypoints',component:AddLoyaltyPointsComponent},
    {path: 'editloyalitypoints/:id',component: AddLoyaltyPointsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoyaltyPointsRoutingModule { }
