import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLoyaltyPointsComponent } from './add-loyalty-points.component';

describe('AddLoyaltyPointsComponent', () => {
  let component: AddLoyaltyPointsComponent;
  let fixture: ComponentFixture<AddLoyaltyPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLoyaltyPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLoyaltyPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
