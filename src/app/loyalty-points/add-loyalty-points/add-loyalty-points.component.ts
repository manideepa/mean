import { Component, OnInit, ViewChild,  ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { LoyaltyPointsServiceService } from "../../services/loyalty-points-service.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-add-loyalty-points',
  templateUrl: './add-loyalty-points.component.html',
  styleUrls: ['./add-loyalty-points.component.scss']
})
export class AddLoyaltyPointsComponent implements OnInit {

  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  loyaltyPointsForm: FormGroup;
  currentUrl = window.location.pathname.split("/");
  base64textString;
  userImg;
 imageData
  constructor(
  private fb: FormBuilder,
    private loyaltyPointsService: LoyaltyPointsServiceService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
	  
  this.loyaltyPointsForm = this.fb.group({
      id: [""],
      price: ["", Validators.required],
      points: ["", Validators.required]
      
    });

    if (this.currentUrl.includes("editloyalitypoints")) {
      let id = this.currentUrl[this.currentUrl.length - 1];
      console.log(id);
      this.loyaltyPointsService.getLoyaltyPointsById(id).subscribe(data => {
        let loyaltyPointsData = data;
        console.log(loyaltyPointsData);
        this.loyaltyPointsForm.patchValue({
          id: loyaltyPointsData._id,
          price: loyaltyPointsData.price,
          points: loyaltyPointsData.points,
          
        });
      });
    }
  }
  
    addloyalitypoints() {
    if (this.currentUrl.includes("editloyalitypoints"))  {
    console.log(this.loyaltyPointsForm.value);
      this.loyaltyPointsService.updateLoyaltyPoints(this.loyaltyPointsForm.value).subscribe(data => {
        this.router.navigate(["/loyalty-points"]);
      }) 
    } else {
		    console.log(this.loyaltyPointsForm.value);
      this.loyaltyPointsService.addLoyaltyPoints(this.loyaltyPointsForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(["/loyalty-points"]);
      });
    }
    
  }

  

}
