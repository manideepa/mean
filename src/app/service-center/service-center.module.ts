import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddServiceCenterComponent } from './add-service-center/add-service-center.component';
import { ServiceCenterManagementComponent } from './service-center-management/service-center-management.component';
import { ServiceCenterRoutingModule } from './service-center-management-routing.module';
import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceCenterServiceService} from './../services/service-center-service.service';
import { HttpClientModule } from '@angular/common/http';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
@NgModule({
  declarations: [AddServiceCenterComponent, ServiceCenterManagementComponent],
  imports: [
    CommonModule,
    ServiceCenterRoutingModule,
      MaterialModule,
      HttpClientModule,
      ReactiveFormsModule,
      GooglePlaceModule
    ],
    exports: [
      MaterialModule
    ],
    providers: [ServiceCenterServiceService]
})
export class ServiceCenterModule { }
