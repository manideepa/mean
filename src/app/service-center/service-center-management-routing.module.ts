import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceCenterManagementComponent } from './service-center-management/service-center-management.component';
import { AddServiceCenterComponent } from './add-service-center/add-service-center.component';


const routes: Routes = [
    {path: '', component: ServiceCenterManagementComponent},
    {path: 'addServiceCenter',component:AddServiceCenterComponent},
    {path: 'editServiceCenter/:id',component: AddServiceCenterComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceCenterRoutingModule { }
