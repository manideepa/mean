import { Component, OnInit ,ViewChild, ElementRef} from '@angular/core';
import { ServiceCenterServiceService } from '../../services/service-center-service.service';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { GooglePlaceModule,GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { BrandServiceService }  from '../../services/brand-service.service';
import { DomSanitizer } from "@angular/platform-browser";


@Component({
  selector: 'app-add-service-center',
  templateUrl: './add-service-center.component.html',
  styleUrls: ['./add-service-center.component.scss']
})

export class AddServiceCenterComponent implements OnInit {
  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  serviceCenterForm: FormGroup;
  addressObj;
  brands=[];
  imageData;
  userImg;
  brandImg;
  base64textString;
  currentUrl = window.location.pathname.split("/");
  constructor(private fb: FormBuilder,private serviceCenter: ServiceCenterServiceService,private router: Router,private brandService:BrandServiceService, private sanitizer: DomSanitizer,) {
		this.serviceCenterForm = this.fb.group({
			  brands: ['']
			});
	  }
 
    

  ngOnInit() {
    this.serviceCenterForm = this.fb.group({
      id: [""],
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      login: ["", Validators.required],
      email: ["", Validators.required],
	    password: ["", Validators.required],
		brands:["", Validators.required],
      imgData: [],
    });
	
	this.getBrands();
	
	    if (this.currentUrl.includes("editServiceCenter")) {
			  let id = this.currentUrl[this.currentUrl.length - 1];
			  this.serviceCenter.getServiceCenterById(id).subscribe(data => {
				let serviceCenterData = data;
				this.imageData = serviceCenterData.imgData;
				this.serviceCenterForm.patchValue({
				  id: serviceCenterData.id,
				  firstName: serviceCenterData.firstName,
				  lastName: serviceCenterData.lastName,
				  login: serviceCenterData.login,
				  email: serviceCenterData.email,
				  brands: serviceCenterData.brands,
					  password: serviceCenterData.password,
				  imgData: this.userImg = this.sanitizer.bypassSecurityTrustUrl("data:image/*;base64," + serviceCenterData.imgData)
				});
			  });
    }
	
  }
  handleAddressChange(e) {
      let longitude = e.geometry.location.lng();
      let lattitude = e.geometry.location.lat();
      let address = e.formatted_address;
      this.addressObj = {
        longitude,
        lattitude,
        address
      }
      console.log("Address",this.addressObj);
  }
  addServiceCenter() {
    let formValues = this.serviceCenterForm.value;
    let formObject = Object.assign(formValues,this.addressObj);
    console.log(formObject);
    this.serviceCenter.addServiceCenter(formObject).subscribe(data => {
      console.log(data);
      this.router.navigate(["/service-center"]);
    })
  }
  getBrands() {
      this.brandService.getBrands().subscribe(data => {
      this.brands = data;
     })
  }
    onFileInput(e) {
    let file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    let imgBaseCode = btoa(binaryString);
    this.serviceCenterForm.patchValue({
      imgData: imgBaseCode
    });
    this.brandImg = this.sanitizer.bypassSecurityTrustUrl(
      "data:image/*;base64," + imgBaseCode
    );
  }
  deleteImg() {
    this.serviceCenterForm.get("imgData").setValue("");
    this.brandImg = "";
    this.fileInput.nativeElement.value = "";
  }
}
