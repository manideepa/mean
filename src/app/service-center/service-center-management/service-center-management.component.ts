import { Component, OnInit } from '@angular/core';
import { ServiceCenterServiceService } from '../../services/service-center-service.service';
@Component({
  selector: 'app-service-center-management',
  templateUrl: './service-center-management.component.html',
  styleUrls: ['./service-center-management.component.scss']
})
export class ServiceCenterManagementComponent implements OnInit {
  serviceCenters;
  constructor(private serviceCenter: ServiceCenterServiceService) { }
 
  ngOnInit() {
      this.serviceCenter.getServiceCenters().subscribe(data=> {
        this.serviceCenters = data;
		console.log(this.serviceCenters);
      })
  }
  
    deleteServiceCenter(id,i) {
    this.serviceCenters.splice(i,1)
    this.serviceCenter.deleteServiceCenter(id).subscribe(data => {
    //  console.log(data);
      
     // console.log("lIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIi",this.brandsList)
    })
  }

}
