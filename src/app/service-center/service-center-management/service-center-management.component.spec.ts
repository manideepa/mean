import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceCenterManagementComponent } from './service-center-management.component';

describe('ServiceCenterManagementComponent', () => {
  let component: ServiceCenterManagementComponent;
  let fixture: ComponentFixture<ServiceCenterManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceCenterManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCenterManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
