import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { UserServiceService }  from '../../services/user-service.service';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm : FormGroup
  isAuthError: boolean = false;
  constructor(private fb: FormBuilder,private userService: UserServiceService,private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['',Validators.required],
      password: ['',Validators.required]
    })
  }
  login() {
    this.isAuthError = false;
    this.userService.checkUser(this.loginForm.value).subscribe((data)=> {
      console.log(data);
      localStorage.setItem('token',data);
      this.router.navigate(['']);
    }, (errror) => {
      this.isAuthError = true;
    })
  }
}
