const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,Content-Type, Accept ,Cache-Control , Authorization , sid");
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
});

//Data base connection
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/warrantyme", { useNewUrlParser: true });
var db = mongoose.connection;

const facultyRouter = require("./api/routes/user.router");
app.use("/users", facultyRouter)

const retailerRouter = require("./api/routes/retailer.router");
app.use('/retailers', retailerRouter);

const productRouter = require("./api/routes/product.router");
app.use('/products', productRouter);

const brandRouter = require("./api/routes/brand.router");
app.use('/brands', brandRouter);

const salesRouter = require("./api/routes/sales.router");
app.use('/sales', salesRouter);

const executiveRouter = require("./api/routes/executive.router");
app.use('/executives', executiveRouter);

const loyaltyPointsRouter = require("./api/routes/loyalty-points.router");
app.use('/loyalty-points', loyaltyPointsRouter);

const customerRouter = require("./api/routes/customer.router");
app.use('/customers', customerRouter);


const serviceCenterRouter = require("./api/routes/service-center.router");
app.use('/serviceCenter', serviceCenterRouter);

const faqRouter = require("./api/routes/faq.router");
app.use('/faq', faqRouter);

const offersRouter = require("./api/routes/offers.router");
app.use('/offers', offersRouter);

const warrantyRequestsRouter = require("./api/routes/warranty-requests.router");
app.use('/warrantyRequests', warrantyRequestsRouter);

const reportsRouter = require("./api/routes/reports.router");
app.use('/reports', reportsRouter);

const categoryRouter = require("./api/routes/category.router");
app.use('/category', categoryRouter);


app.listen("3030","0.0.0.0", (req, res) => {
    console.log("Server running at 3030")
});